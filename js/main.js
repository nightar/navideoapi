/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);


jQuery(function () {
  var video = new naVideoApi('#vimeo', {
    playerVars: {
      controls: 0,
      disablekb: 1,
      enablejsapi: 1,
      modestbranding: 1,
      showinfo: 0
    }
  });
  video.setLoop();
  var videoY = new naVideoApi('#youtube', {
    playerVars: {
      controls: 0,
      disablekb: 1,
      enablejsapi: 1,
      modestbranding: 1,
      showinfo: 0
    }
  });
  videoY.setLoop();
  var actions = ['setSize', 'play', 'pause', 'play', 'stop', 'play', 'mute', 'unMute', 'seekTo', 'setVolume'];
  var i = 0;
  var timer = setInterval(function () {
    if (i < actions.length) {
      var action = actions[i++];
      react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(action, document.getElementsByClassName('action-note')[0]);

      if (action == 'seekTo') {
        video[action](90);
        videoY[action](90);
      } else if (action == 'setVolume') {
        video[action](40);
        videoY[action](40);
      } else if (action == 'setSize') {
        video[action](300, 300);
        videoY[action](300, 300);
      } else {
        video[action]();
        videoY[action]();
      }
    } else {
      clearInterval(timer);
    }
  }, 5000);
});

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "React" ***!
  \************************/
/***/ (function(module, exports) {

module.exports = React;

/***/ }),

/***/ "react-dom":
/*!***************************!*\
  !*** external "ReactDOM" ***!
  \***************************/
/***/ (function(module, exports) {

module.exports = ReactDOM;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIiwid2VicGFjazovLy9leHRlcm5hbCBcIlJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiUmVhY3RET01cIiJdLCJuYW1lcyI6WyJqUXVlcnkiLCJ2aWRlbyIsIm5hVmlkZW9BcGkiLCJwbGF5ZXJWYXJzIiwiY29udHJvbHMiLCJkaXNhYmxla2IiLCJlbmFibGVqc2FwaSIsIm1vZGVzdGJyYW5kaW5nIiwic2hvd2luZm8iLCJzZXRMb29wIiwidmlkZW9ZIiwiYWN0aW9ucyIsImkiLCJ0aW1lciIsInNldEludGVydmFsIiwibGVuZ3RoIiwiYWN0aW9uIiwiUmVhY3RET00iLCJyZW5kZXIiLCJkb2N1bWVudCIsImdldEVsZW1lbnRzQnlDbGFzc05hbWUiLCJjbGVhckludGVydmFsIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQUEsTUFBTSxDQUFDLFlBQU07QUFDWCxNQUFNQyxLQUFLLEdBQUcsSUFBSUMsVUFBSixDQUFnQixRQUFoQixFQUEwQjtBQUN0Q0MsY0FBVSxFQUFFO0FBQ1ZDLGNBQVEsRUFBRSxDQURBO0FBRVZDLGVBQVMsRUFBRSxDQUZEO0FBR1ZDLGlCQUFXLEVBQUUsQ0FISDtBQUlWQyxvQkFBYyxFQUFFLENBSk47QUFLVkMsY0FBUSxFQUFFO0FBTEE7QUFEMEIsR0FBMUIsQ0FBZDtBQVNBUCxPQUFLLENBQUNRLE9BQU47QUFDQSxNQUFNQyxNQUFNLEdBQUcsSUFBSVIsVUFBSixDQUFnQixVQUFoQixFQUE0QjtBQUN6Q0MsY0FBVSxFQUFFO0FBQ1ZDLGNBQVEsRUFBRSxDQURBO0FBRVZDLGVBQVMsRUFBRSxDQUZEO0FBR1ZDLGlCQUFXLEVBQUUsQ0FISDtBQUlWQyxvQkFBYyxFQUFFLENBSk47QUFLVkMsY0FBUSxFQUFFO0FBTEE7QUFENkIsR0FBNUIsQ0FBZjtBQVNBRSxRQUFNLENBQUNELE9BQVA7QUFDQSxNQUFNRSxPQUFPLEdBQUcsQ0FDZCxTQURjLEVBRWQsTUFGYyxFQUdkLE9BSGMsRUFJZCxNQUpjLEVBS2QsTUFMYyxFQU1kLE1BTmMsRUFPZCxNQVBjLEVBUWQsUUFSYyxFQVNkLFFBVGMsRUFVZCxXQVZjLENBQWhCO0FBWUEsTUFBSUMsQ0FBQyxHQUFHLENBQVI7QUFDQSxNQUFNQyxLQUFLLEdBQUdDLFdBQVcsQ0FBQyxZQUFNO0FBQzlCLFFBQUlGLENBQUMsR0FBR0QsT0FBTyxDQUFDSSxNQUFoQixFQUF3QjtBQUN0QixVQUFNQyxNQUFNLEdBQUdMLE9BQU8sQ0FBQ0MsQ0FBQyxFQUFGLENBQXRCO0FBQ0FLLHNEQUFRLENBQUNDLE1BQVQsQ0FBaUJGLE1BQWpCLEVBQXlCRyxRQUFRLENBQUNDLHNCQUFULENBQWlDLGFBQWpDLEVBQWlELENBQWpELENBQXpCOztBQUNBLFVBQUtKLE1BQU0sSUFBSSxRQUFmLEVBQTBCO0FBQ3hCZixhQUFLLENBQUNlLE1BQUQsQ0FBTCxDQUFlLEVBQWY7QUFDQU4sY0FBTSxDQUFDTSxNQUFELENBQU4sQ0FBZ0IsRUFBaEI7QUFDRCxPQUhELE1BR08sSUFBS0EsTUFBTSxJQUFJLFdBQWYsRUFBNkI7QUFDbENmLGFBQUssQ0FBQ2UsTUFBRCxDQUFMLENBQWUsRUFBZjtBQUNBTixjQUFNLENBQUNNLE1BQUQsQ0FBTixDQUFnQixFQUFoQjtBQUNELE9BSE0sTUFHQSxJQUFLQSxNQUFNLElBQUksU0FBZixFQUEyQjtBQUNoQ2YsYUFBSyxDQUFDZSxNQUFELENBQUwsQ0FBZSxHQUFmLEVBQW9CLEdBQXBCO0FBQ0FOLGNBQU0sQ0FBQ00sTUFBRCxDQUFOLENBQWdCLEdBQWhCLEVBQXFCLEdBQXJCO0FBQ0QsT0FITSxNQUdBO0FBQ0xmLGFBQUssQ0FBQ2UsTUFBRCxDQUFMO0FBQ0FOLGNBQU0sQ0FBQ00sTUFBRCxDQUFOO0FBQ0Q7QUFDRixLQWhCRCxNQWdCTztBQUNOSyxtQkFBYSxDQUFFUixLQUFGLENBQWI7QUFDQTtBQUNGLEdBcEJ3QixFQW9CdEIsSUFwQnNCLENBQXpCO0FBcUJELENBdkRLLENBQU4sQzs7Ozs7Ozs7OztBQ0hBLHVCOzs7Ozs7Ozs7O0FDQUEsMEIiLCJmaWxlIjoianMvbWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJ1xyXG5cclxualF1ZXJ5KCgpID0+IHtcclxuICBjb25zdCB2aWRlbyA9IG5ldyBuYVZpZGVvQXBpKCAnI3ZpbWVvJywge1xyXG4gICAgcGxheWVyVmFyczoge1xyXG4gICAgICBjb250cm9sczogMCxcclxuICAgICAgZGlzYWJsZWtiOiAxLFxyXG4gICAgICBlbmFibGVqc2FwaTogMSxcclxuICAgICAgbW9kZXN0YnJhbmRpbmc6IDEsXHJcbiAgICAgIHNob3dpbmZvOiAwLFxyXG4gICAgfVxyXG4gIH0pO1xyXG4gIHZpZGVvLnNldExvb3AoKTtcclxuICBjb25zdCB2aWRlb1kgPSBuZXcgbmFWaWRlb0FwaSggJyN5b3V0dWJlJywge1xyXG4gICAgcGxheWVyVmFyczoge1xyXG4gICAgICBjb250cm9sczogMCxcclxuICAgICAgZGlzYWJsZWtiOiAxLFxyXG4gICAgICBlbmFibGVqc2FwaTogMSxcclxuICAgICAgbW9kZXN0YnJhbmRpbmc6IDEsXHJcbiAgICAgIHNob3dpbmZvOiAwLFxyXG4gICAgfVxyXG4gIH0pO1xyXG4gIHZpZGVvWS5zZXRMb29wKCk7XHJcbiAgY29uc3QgYWN0aW9ucyA9IFtcclxuICAgICdzZXRTaXplJyxcclxuICAgICdwbGF5JyxcclxuICAgICdwYXVzZScsXHJcbiAgICAncGxheScsXHJcbiAgICAnc3RvcCcsXHJcbiAgICAncGxheScsXHJcbiAgICAnbXV0ZScsXHJcbiAgICAndW5NdXRlJyxcclxuICAgICdzZWVrVG8nLFxyXG4gICAgJ3NldFZvbHVtZScsXHJcbiAgXVxyXG4gIGxldCBpID0gMDtcclxuICBjb25zdCB0aW1lciA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgIGlmIChpIDwgYWN0aW9ucy5sZW5ndGgpIHtcclxuICAgICAgY29uc3QgYWN0aW9uID0gYWN0aW9uc1tpKytdO1xyXG4gICAgICBSZWFjdERPTS5yZW5kZXIoIGFjdGlvbiwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSggJ2FjdGlvbi1ub3RlJyApWzBdKVxyXG4gICAgICBpZiAoIGFjdGlvbiA9PSAnc2Vla1RvJyApIHtcclxuICAgICAgICB2aWRlb1thY3Rpb25dKCA5MCApO1xyXG4gICAgICAgIHZpZGVvWVthY3Rpb25dKCA5MCApO1xyXG4gICAgICB9IGVsc2UgaWYgKCBhY3Rpb24gPT0gJ3NldFZvbHVtZScgKSB7XHJcbiAgICAgICAgdmlkZW9bYWN0aW9uXSggNDAgKTtcclxuICAgICAgICB2aWRlb1lbYWN0aW9uXSggNDAgKTtcclxuICAgICAgfSBlbHNlIGlmICggYWN0aW9uID09ICdzZXRTaXplJyApIHtcclxuICAgICAgICB2aWRlb1thY3Rpb25dKCAzMDAsIDMwMCApO1xyXG4gICAgICAgIHZpZGVvWVthY3Rpb25dKCAzMDAsIDMwMCApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHZpZGVvW2FjdGlvbl0oKTtcclxuICAgICAgICB2aWRlb1lbYWN0aW9uXSgpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgIGNsZWFySW50ZXJ2YWwoIHRpbWVyICk7XHJcbiAgICB9XHJcbiAgfSwgNTAwMClcclxufSkiLCJtb2R1bGUuZXhwb3J0cyA9IFJlYWN0OyIsIm1vZHVsZS5leHBvcnRzID0gUmVhY3RET007Il0sInNvdXJjZVJvb3QiOiIifQ==