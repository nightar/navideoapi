/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/dev/navideoapi.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/dev/navideoapi.js":
/*!*******************************!*\
  !*** ./src/dev/navideoapi.js ***!
  \*******************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _providers_youtube__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providers/youtube */ "./src/dev/providers/youtube.js");
/* harmony import */ var _providers_vimeo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providers/vimeo */ "./src/dev/providers/vimeo.js");



(function (w) {
  var naVideoApiData = {
    youtube: _providers_youtube__WEBPACK_IMPORTED_MODULE_0__["default"],
    vimeo: _providers_vimeo__WEBPACK_IMPORTED_MODULE_1__["default"]
  };

  w.naVideoApi = function (video, options) {
    if (video) {
      this.video = video;
      this.options = options;
      this.init();
      return this;
    } else {
      return null;
    }
  };

  naVideoApi.prototype = {
    provider: null,

    /**
     * Init provider
     */
    init: function init() {
      for (var providerName in naVideoApiData) {
        var provider = new naVideoApiData[providerName]();

        if (provider.load(this)) {
          this.provider = provider;
        }
      }
    },

    /**
     * Get html video
     */
    getTpl: function getTpl() {
      if (this.provider && this.provider.tpl && this.embededUrl) {
        return this.provider.tpl.replace('$url', this.embededUrl);
      }
    },

    /**
     * Video controls
     */
    play: function play() {
      if (this.provider) {
        this.provider.play();
      }
    },
    pause: function pause() {
      if (this.provider) {
        this.provider.pause();
      }
    },
    stop: function stop() {
      if (this.provider) {
        this.provider.stop();
      }
    },
    mute: function mute() {
      if (this.provider) {
        this.provider.mute();
      }
    },
    unMute: function unMute() {
      if (this.provider) {
        this.provider.unMute();
      }
    },
    destroy: function destroy() {
      if (this.provider) {
        this.provider.destroy();
      }

      this.options = null;
    },
    seekTo: function seekTo(seconds) {
      if (!seconds) {
        seconds = 0;
      }

      if (this.provider) {
        this.provider.seekTo(seconds);
      }
    },
    setVolume: function setVolume(level) {
      if (!level) {
        level = 1;
      }

      if (this.provider) {
        this.provider.setVolume(level);
      }
    },
    setSize: function setSize(w, h) {
      if (!w) {
        w = 340;
      }

      if (!h) {
        h = 480;
      }

      if (this.provider) {
        this.provider.setSize(w, h);
      }
    },
    setLoop: function setLoop() {
      var bool = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

      if (this.provider) {
        this.provider.setLoop(bool);
      }
    }
  };
})(window);

/* harmony default export */ __webpack_exports__["default"] = (naVideoApi);

/***/ }),

/***/ "./src/dev/providers/base.js":
/*!***********************************!*\
  !*** ./src/dev/providers/base.js ***!
  \***********************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var base =
/*#__PURE__*/
function () {
  _createClass(base, [{
    key: "init",
    value: function init() {
      this.match = /^([\d]+)?/;
      this.idMatchIndex = 1;
      this.url = "https://www.example.com/$id?api=1";
      this.tpl = "<iframe  src=\"$url\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
      this.api = "https://www.example.com/api";
      this.id = null;
      this.container = null;
      this.player = null;
      this.status = {
        loaded: false
      };
    }
  }]);

  function base() {
    _classCallCheck(this, base);

    this.init();
  }

  _createClass(base, [{
    key: "load",
    value: function load(plugin) {
      var _this = this;

      var element;
      var src;
      var options = Object.assign({}, plugin.options);

      if (plugin.video instanceof Element || plugin.video instanceof HTMLDocument) {
        element = plugin.video;
      } else if (typeof plugin.video == "string") {
        element = document.querySelector(plugin.video);
      }

      if (element) {
        if (element.tagName == 'IFRAME') {
          src = element.getAttribute('src');
        } else if (typeof options.url == "string") {
          src = options.url;
        } else if (typeof options.videoId == "string") {
          this.id = options.videoId;
        }

        if (src) {
          var r = src.match(this.match);

          if (r) {
            this.id = r[this.idMatchIndex];
            options.videoId = r[this.idMatchIndex];
          }
        }
      }

      if (this.id) {
        if (element && element.tagName == 'IFRAME') {
          this.video = element;
          element.setAttribute('src', this.url.replace('$id', this.id));
        } else if (element) {
          this.container = element;
        }

        if (!this.class || typeof window[this.class] == "undefined" || !window[this.class]) {
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.src = this.api;

          script.onload = function () {
            _this.status.loaded = true;
            delete options.url;

            _this.getPlayer(options);
          };

          document.body.appendChild(script);
        } else {
          this.status.loaded = true;
          delete options.url;
          this.getPlayer(options);
        }

        return this;
      }

      return null;
    }
  }, {
    key: "play",
    value: function play() {}
  }, {
    key: "pause",
    value: function pause() {}
  }, {
    key: "stop",
    value: function stop() {}
  }, {
    key: "mute",
    value: function mute() {}
  }, {
    key: "destroy",
    value: function destroy() {}
  }, {
    key: "unMute",
    value: function unMute() {}
  }, {
    key: "seekTo",
    value: function seekTo(seconds) {}
  }, {
    key: "setVolume",
    value: function setVolume(number) {}
  }, {
    key: "setSize",
    value: function setSize(w, h) {}
  }, {
    key: "setLoop",
    value: function setLoop(bool) {}
  }, {
    key: "getPlayer",
    value: function getPlayer(options) {}
  }]);

  return base;
}();

/* harmony default export */ __webpack_exports__["default"] = (base);

/***/ }),

/***/ "./src/dev/providers/vimeo.js":
/*!************************************!*\
  !*** ./src/dev/providers/vimeo.js ***!
  \************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "./src/dev/providers/base.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var vimeo =
/*#__PURE__*/
function (_base) {
  _inherits(vimeo, _base);

  function vimeo() {
    _classCallCheck(this, vimeo);

    return _possibleConstructorReturn(this, _getPrototypeOf(vimeo).apply(this, arguments));
  }

  _createClass(vimeo, [{
    key: "init",
    value: function init() {
      this.match = /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/;
      this.idMatchIndex = 2;
      this.url = "https://player.vimeo.com/video/$id?api=1";
      this.tpl = "<iframe  src=\"$url\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
      this.api = "https://player.vimeo.com/api/player.js";
      this.class = "https://player.vimeo.com/api/player.js";
      this.id = null;
      this.container = null;
      this.player = null;
      this.timeout = 100;
      this.volume = 0;
      this.status = {
        loaded: false
      };
    }
  }, {
    key: "getPlayer",
    value: function getPlayer(options) {
      var _this = this;

      if (this.id) {
        var timeout = setInterval(function () {
          if (_this.status.loaded) {
            clearInterval(timeout);

            if (_this.video) {
              _this.player = new Vimeo.Player(_this.video, options);
            } else if (_this.container) {
              options.id = _this.id;
              _this.player = new Vimeo.Player(_this.container, options);
              _this.video = _this.player.element;
            }
          }
        }, this.timeout);
      }
    }
  }, {
    key: "play",
    value: function play() {
      var _this2 = this;

      var timeout = setInterval(function () {
        if (_this2.player && typeof _this2.player.play == "function") {
          _this2.player.play();

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (this.player && typeof this.player.destroy == "function") {
        this.player.destroy();
      }
    }
  }, {
    key: "pause",
    value: function pause() {
      var _this3 = this;

      var timeout = setInterval(function () {
        if (_this3.player && typeof _this3.player.pause == "function") {
          _this3.player.pause();

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "stop",
    value: function stop() {
      var _this4 = this;

      var timeout = setInterval(function () {
        if (_this4.player && typeof _this4.player.pause == "function") {
          _this4.player.pause();

          _this4.player.setCurrentTime(0);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "mute",
    value: function mute() {
      var _this5 = this;

      var timeout = setInterval(function () {
        if (_this5.player && typeof _this5.player.setVolume == "function") {
          _this5.player.getVolume().then(function (volume) {
            _this5.volume = volume;
          });

          _this5.player.setVolume(0);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "unMute",
    value: function unMute() {
      var _this6 = this;

      var timeout = setInterval(function () {
        if (_this6.player && typeof _this6.player.setVolume == "function") {
          if (_this6.volume == 0) {
            _this6.volume = 1;
          }

          _this6.player.setVolume(_this6.volume);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "seekTo",
    value: function seekTo(seconds) {
      var _this7 = this;

      var timeout = setInterval(function () {
        if (_this7.player && typeof _this7.player.setCurrentTime == "function") {
          _this7.player.setCurrentTime(seconds);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "setVolume",
    value: function setVolume(number) {
      var _this8 = this;

      var timeout = setInterval(function () {
        if (_this8.player && typeof _this8.player.setVolume == "function") {
          var volume = number > 100 ? 1 : number / 100;

          _this8.player.setVolume(volume);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "setLoop",
    value: function setLoop(bool) {
      var _this9 = this;

      var timeout = setInterval(function () {
        if (_this9.player && typeof _this9.player.setLoop == "function") {
          _this9.player.setLoop(bool);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "setSize",
    value: function setSize(w, h) {
      var _this10 = this;

      var timeout = setInterval(function () {
        if (_this10.player && typeof _this10.player.element != "undefined") {
          _this10.player.element.setAttribute('width', w);

          _this10.player.element.setAttribute('height', h);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }]);

  return vimeo;
}(_base__WEBPACK_IMPORTED_MODULE_0__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (vimeo);

/***/ }),

/***/ "./src/dev/providers/youtube.js":
/*!**************************************!*\
  !*** ./src/dev/providers/youtube.js ***!
  \**************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "./src/dev/providers/base.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var youtube =
/*#__PURE__*/
function (_base) {
  _inherits(youtube, _base);

  function youtube() {
    _classCallCheck(this, youtube);

    return _possibleConstructorReturn(this, _getPrototypeOf(youtube).apply(this, arguments));
  }

  _createClass(youtube, [{
    key: "init",
    value: function init() {
      this.match = /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i;
      this.idMatchIndex = 4;
      this.url = "https://www.youtube-nocookie.com/embed/$id?enablejsapi=1";
      this.tpl = "<iframe  src=\"$url\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
      this.api = "https://www.youtube.com/iframe_api";
      this.id = null;
      this.container = null;
      this.player = null;
      this.timeout = 100;
      this.loop = 0;
      this.status = {
        loaded: false
      };
    }
  }, {
    key: "load",
    value: function load(plugin) {
      var _this = this;

      var element;
      var src;
      var options = Object.assign({}, plugin.options);

      if (plugin.video instanceof Element || plugin.video instanceof HTMLDocument) {
        element = plugin.video;
      } else if (typeof plugin.video == "string") {
        element = document.querySelector(plugin.video);
      }

      if (element) {
        if (element.tagName == 'IFRAME') {
          src = element.getAttribute('src');
        } else if (typeof options.url == "string") {
          src = options.url;
        } else if (typeof options.videoId == "string") {
          this.id = options.videoId;
        }

        if (src) {
          var r = src.match(this.match);

          if (r) {
            this.id = r[this.idMatchIndex];
            options.videoId = r[this.idMatchIndex];
          }
        }
      }

      if (this.id) {
        if (element && element.tagName == 'IFRAME') {
          this.video = element;
          element.setAttribute('src', this.url.replace('$id', this.id));
        } else if (element) {
          this.container = element;
        }

        if (typeof YT == "undefined" || !YT) {
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.src = this.api;

          window.onYouTubeIframeAPIReady = function () {
            _this.status.loaded = true;
            delete options.url;

            _this.getPlayer(options);
          };

          document.body.appendChild(script);
        } else {
          this.status.loaded = true;
          delete options.url;
          this.getPlayer(options);
        }

        return this;
      }

      return null;
    }
  }, {
    key: "play",
    value: function play() {
      var _this2 = this;

      var timeout = setInterval(function () {
        if (_this2.player && typeof _this2.player.playVideo == "function") {
          _this2.player.playVideo();

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "pause",
    value: function pause() {
      var _this3 = this;

      var timeout = setInterval(function () {
        if (_this3.player && typeof _this3.player.pauseVideo == "function") {
          _this3.player.pauseVideo();

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "stop",
    value: function stop() {
      var _this4 = this;

      var timeout = setInterval(function () {
        if (_this4.player && typeof _this4.player.stopVideo == "function") {
          _this4.player.stopVideo();

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (this.player && typeof this.player.destroy == "function") {
        this.player.destroy();
      }
    }
  }, {
    key: "mute",
    value: function mute() {
      var _this5 = this;

      var timeout = setInterval(function () {
        if (_this5.player && typeof _this5.player.mute == "function") {
          _this5.player.mute();

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "unMute",
    value: function unMute() {
      var _this6 = this;

      var timeout = setInterval(function () {
        if (_this6.player && typeof _this6.player.unMute == "function") {
          _this6.player.unMute();

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "seekTo",
    value: function seekTo(seconds) {
      var _this7 = this;

      var timeout = setInterval(function () {
        if (_this7.player && typeof _this7.player.seekTo == "function") {
          _this7.player.seekTo(seconds);

          clearInterval(timeout);
        }
      }, timeout);
    }
  }, {
    key: "setVolume",
    value: function setVolume(number) {
      var _this8 = this;

      var timeout = setInterval(function () {
        if (_this8.player && typeof _this8.player.setVolume == "function") {
          _this8.player.setVolume(number);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "setSize",
    value: function setSize(w, h) {
      var _this9 = this;

      var timeout = setInterval(function () {
        if (_this9.player && typeof _this9.player.setSize == "function") {
          _this9.player.a.setAttribute('width', w);

          _this9.player.a.setAttribute('height', h);

          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "getPlayer",
    value: function getPlayer(options) {
      var _this10 = this;

      if (this.id) {
        options.events = {
          'onStateChange': function onStateChange(e) {
            if (YT.PlayerState.ENDED == e.data) {
              _this10.doLoop();
            }
          }
        };
        var timeout = setInterval(function () {
          if (_this10.status.loaded) {
            clearInterval(timeout);

            if (_this10.video) {
              _this10.player = new YT.Player(_this10.video, options);
            } else if (_this10.container) {
              _this10.player = new YT.Player(_this10.container, options);
              _this10.video = _this10.player.a;
            }
          }
        }, this.timeout);
      }
    }
  }, {
    key: "setLoop",
    value: function setLoop(bool) {
      var _this11 = this;

      var timeout = setInterval(function () {
        if (_this11.player) {
          _this11.loop = bool;
          clearInterval(timeout);
        }
      }, this.timeout);
    }
  }, {
    key: "doLoop",
    value: function doLoop() {
      if (this.player && this.loop) {
        this.player.seekTo(0);
      }
    }
  }]);

  return youtube;
}(_base__WEBPACK_IMPORTED_MODULE_0__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (youtube);

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Rldi9uYXZpZGVvYXBpLmpzIiwid2VicGFjazovLy8uL3NyYy9kZXYvcHJvdmlkZXJzL2Jhc2UuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Rldi9wcm92aWRlcnMvdmltZW8uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Rldi9wcm92aWRlcnMveW91dHViZS5qcyJdLCJuYW1lcyI6WyJ3IiwibmFWaWRlb0FwaURhdGEiLCJ5b3V0dWJlIiwidmltZW8iLCJuYVZpZGVvQXBpIiwidmlkZW8iLCJvcHRpb25zIiwiaW5pdCIsInByb3RvdHlwZSIsInByb3ZpZGVyIiwicHJvdmlkZXJOYW1lIiwibG9hZCIsImdldFRwbCIsInRwbCIsImVtYmVkZWRVcmwiLCJyZXBsYWNlIiwicGxheSIsInBhdXNlIiwic3RvcCIsIm11dGUiLCJ1bk11dGUiLCJkZXN0cm95Iiwic2Vla1RvIiwic2Vjb25kcyIsInNldFZvbHVtZSIsImxldmVsIiwic2V0U2l6ZSIsImgiLCJzZXRMb29wIiwiYm9vbCIsIndpbmRvdyIsImJhc2UiLCJtYXRjaCIsImlkTWF0Y2hJbmRleCIsInVybCIsImFwaSIsImlkIiwiY29udGFpbmVyIiwicGxheWVyIiwic3RhdHVzIiwibG9hZGVkIiwicGx1Z2luIiwiZWxlbWVudCIsInNyYyIsIk9iamVjdCIsImFzc2lnbiIsIkVsZW1lbnQiLCJIVE1MRG9jdW1lbnQiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCJ0YWdOYW1lIiwiZ2V0QXR0cmlidXRlIiwidmlkZW9JZCIsInIiLCJzZXRBdHRyaWJ1dGUiLCJjbGFzcyIsInNjcmlwdCIsImNyZWF0ZUVsZW1lbnQiLCJ0eXBlIiwib25sb2FkIiwiZ2V0UGxheWVyIiwiYm9keSIsImFwcGVuZENoaWxkIiwibnVtYmVyIiwidGltZW91dCIsInZvbHVtZSIsInNldEludGVydmFsIiwiY2xlYXJJbnRlcnZhbCIsIlZpbWVvIiwiUGxheWVyIiwic2V0Q3VycmVudFRpbWUiLCJnZXRWb2x1bWUiLCJ0aGVuIiwibG9vcCIsIllUIiwib25Zb3VUdWJlSWZyYW1lQVBJUmVhZHkiLCJwbGF5VmlkZW8iLCJwYXVzZVZpZGVvIiwic3RvcFZpZGVvIiwiYSIsImV2ZW50cyIsImUiLCJQbGF5ZXJTdGF0ZSIsIkVOREVEIiwiZGF0YSIsImRvTG9vcCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7O0FBRUEsQ0FBQyxVQUFDQSxDQUFELEVBQU87QUFDTixNQUFNQyxjQUFjLEdBQUc7QUFDckJDLFdBQU8sRUFBUEEsMERBRHFCO0FBRXJCQyxTQUFLLEVBQUxBLHdEQUFLQTtBQUZnQixHQUF2Qjs7QUFLQUgsR0FBQyxDQUFDSSxVQUFGLEdBQWUsVUFBVUMsS0FBVixFQUFpQkMsT0FBakIsRUFBMEI7QUFDdkMsUUFBSUQsS0FBSixFQUFXO0FBQ1QsV0FBS0EsS0FBTCxHQUFhQSxLQUFiO0FBQ0EsV0FBS0MsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsV0FBS0MsSUFBTDtBQUNBLGFBQU8sSUFBUDtBQUNELEtBTEQsTUFLTztBQUNMLGFBQU8sSUFBUDtBQUNEO0FBQ0YsR0FURDs7QUFVQUgsWUFBVSxDQUFDSSxTQUFYLEdBQXVCO0FBRXJCQyxZQUFRLEVBQUUsSUFGVzs7QUFHckI7OztBQUdBRixRQUFJLEVBQUUsZ0JBQVk7QUFDaEIsV0FBSyxJQUFJRyxZQUFULElBQXlCVCxjQUF6QixFQUF5QztBQUN2QyxZQUFNUSxRQUFRLEdBQUcsSUFBSVIsY0FBYyxDQUFDUyxZQUFELENBQWxCLEVBQWpCOztBQUNBLFlBQUlELFFBQVEsQ0FBQ0UsSUFBVCxDQUFlLElBQWYsQ0FBSixFQUEyQjtBQUN6QixlQUFLRixRQUFMLEdBQWdCQSxRQUFoQjtBQUNEO0FBRUY7QUFDRixLQWRvQjs7QUFlckI7OztBQUdBRyxVQUFNLEVBQUUsa0JBQVk7QUFDbEIsVUFBSSxLQUFLSCxRQUFMLElBQWlCLEtBQUtBLFFBQUwsQ0FBY0ksR0FBL0IsSUFBc0MsS0FBS0MsVUFBL0MsRUFBMkQ7QUFDekQsZUFBTyxLQUFLTCxRQUFMLENBQWNJLEdBQWQsQ0FBa0JFLE9BQWxCLENBQTBCLE1BQTFCLEVBQWtDLEtBQUtELFVBQXZDLENBQVA7QUFDRDtBQUNGLEtBdEJvQjs7QUF1QnJCOzs7QUFHQUUsUUFBSSxFQUFFLGdCQUFZO0FBQ2hCLFVBQUksS0FBS1AsUUFBVCxFQUFtQjtBQUVqQixhQUFLQSxRQUFMLENBQWNPLElBQWQ7QUFDRDtBQUNGLEtBL0JvQjtBQWdDckJDLFNBQUssRUFBRSxpQkFBWTtBQUNqQixVQUFJLEtBQUtSLFFBQVQsRUFBbUI7QUFDakIsYUFBS0EsUUFBTCxDQUFjUSxLQUFkO0FBQ0Q7QUFDRixLQXBDb0I7QUFxQ3JCQyxRQUFJLEVBQUUsZ0JBQVk7QUFDaEIsVUFBSSxLQUFLVCxRQUFULEVBQW1CO0FBQ2pCLGFBQUtBLFFBQUwsQ0FBY1MsSUFBZDtBQUNEO0FBQ0YsS0F6Q29CO0FBMENyQkMsUUFBSSxFQUFFLGdCQUFZO0FBQ2hCLFVBQUksS0FBS1YsUUFBVCxFQUFtQjtBQUNqQixhQUFLQSxRQUFMLENBQWNVLElBQWQ7QUFDRDtBQUNGLEtBOUNvQjtBQStDckJDLFVBQU0sRUFBRSxrQkFBWTtBQUNsQixVQUFJLEtBQUtYLFFBQVQsRUFBbUI7QUFDakIsYUFBS0EsUUFBTCxDQUFjVyxNQUFkO0FBQ0Q7QUFDRixLQW5Eb0I7QUFvRHJCQyxXQUFPLEVBQUUsbUJBQVk7QUFDbkIsVUFBSSxLQUFLWixRQUFULEVBQW1CO0FBQ2pCLGFBQUtBLFFBQUwsQ0FBY1ksT0FBZDtBQUNEOztBQUNELFdBQUtmLE9BQUwsR0FBZSxJQUFmO0FBQ0QsS0F6RG9CO0FBMERyQmdCLFVBQU0sRUFBRSxnQkFBVUMsT0FBVixFQUFtQjtBQUN6QixVQUFJLENBQUNBLE9BQUwsRUFBYztBQUNaQSxlQUFPLEdBQUcsQ0FBVjtBQUNEOztBQUNELFVBQUksS0FBS2QsUUFBVCxFQUFtQjtBQUNqQixhQUFLQSxRQUFMLENBQWNhLE1BQWQsQ0FBcUJDLE9BQXJCO0FBQ0Q7QUFDRixLQWpFb0I7QUFrRXJCQyxhQUFTLEVBQUUsbUJBQVVDLEtBQVYsRUFBaUI7QUFDMUIsVUFBSSxDQUFDQSxLQUFMLEVBQVk7QUFDVkEsYUFBSyxHQUFHLENBQVI7QUFDRDs7QUFDRCxVQUFJLEtBQUtoQixRQUFULEVBQW1CO0FBQ2pCLGFBQUtBLFFBQUwsQ0FBY2UsU0FBZCxDQUF3QkMsS0FBeEI7QUFDRDtBQUNGLEtBekVvQjtBQTBFckJDLFdBQU8sRUFBRSxpQkFBVTFCLENBQVYsRUFBYTJCLENBQWIsRUFBZ0I7QUFDdkIsVUFBSSxDQUFDM0IsQ0FBTCxFQUFRO0FBQ05BLFNBQUMsR0FBRyxHQUFKO0FBQ0Q7O0FBQ0QsVUFBSSxDQUFDMkIsQ0FBTCxFQUFRO0FBQ05BLFNBQUMsR0FBRyxHQUFKO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLbEIsUUFBVCxFQUFtQjtBQUNqQixhQUFLQSxRQUFMLENBQWNpQixPQUFkLENBQXNCMUIsQ0FBdEIsRUFBeUIyQixDQUF6QjtBQUNEO0FBQ0YsS0FwRm9CO0FBcUZyQkMsV0FBTyxFQUFFLG1CQUF5QjtBQUFBLFVBQWRDLElBQWMsdUVBQVAsSUFBTzs7QUFDaEMsVUFBSSxLQUFLcEIsUUFBVCxFQUFtQjtBQUNqQixhQUFLQSxRQUFMLENBQWNtQixPQUFkLENBQXVCQyxJQUF2QjtBQUNEO0FBQ0Y7QUF6Rm9CLEdBQXZCO0FBNEZELENBNUdELEVBNEdHQyxNQTVHSDs7QUE4R2UxQix5RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNqSE0yQixJOzs7OzsyQkFDRztBQUNMLFdBQUtDLEtBQUwsR0FBYSxXQUFiO0FBQ0EsV0FBS0MsWUFBTCxHQUFvQixDQUFwQjtBQUNBLFdBQUtDLEdBQUwsR0FBVyxtQ0FBWDtBQUNBLFdBQUtyQixHQUFMLEdBQVcsOExBQVg7QUFDQSxXQUFLc0IsR0FBTCxHQUFXLDZCQUFYO0FBQ0EsV0FBS0MsRUFBTCxHQUFVLElBQVY7QUFDQSxXQUFLQyxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsV0FBS0MsTUFBTCxHQUFjLElBQWQ7QUFDQSxXQUFLQyxNQUFMLEdBQWM7QUFDWkMsY0FBTSxFQUFFO0FBREksT0FBZDtBQUdEOzs7QUFFRCxrQkFBYztBQUFBOztBQUNaLFNBQUtqQyxJQUFMO0FBQ0Q7Ozs7eUJBRUlrQyxNLEVBQVE7QUFBQTs7QUFDWCxVQUFJQyxPQUFKO0FBQ0EsVUFBSUMsR0FBSjtBQUNBLFVBQUlyQyxPQUFPLEdBQUlzQyxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCSixNQUFNLENBQUNuQyxPQUF6QixDQUFmOztBQUNBLFVBQUltQyxNQUFNLENBQUNwQyxLQUFQLFlBQXdCeUMsT0FBeEIsSUFBbUNMLE1BQU0sQ0FBQ3BDLEtBQVAsWUFBd0IwQyxZQUEvRCxFQUE2RTtBQUMzRUwsZUFBTyxHQUFHRCxNQUFNLENBQUNwQyxLQUFqQjtBQUNELE9BRkQsTUFFTyxJQUFJLE9BQU9vQyxNQUFNLENBQUNwQyxLQUFkLElBQXVCLFFBQTNCLEVBQXFDO0FBQzFDcUMsZUFBTyxHQUFHTSxRQUFRLENBQUNDLGFBQVQsQ0FBdUJSLE1BQU0sQ0FBQ3BDLEtBQTlCLENBQVY7QUFDRDs7QUFDRCxVQUFJcUMsT0FBSixFQUFhO0FBQ1gsWUFBSUEsT0FBTyxDQUFDUSxPQUFSLElBQW1CLFFBQXZCLEVBQWlDO0FBQy9CUCxhQUFHLEdBQUdELE9BQU8sQ0FBQ1MsWUFBUixDQUFxQixLQUFyQixDQUFOO0FBQ0QsU0FGRCxNQUVPLElBQUksT0FBTzdDLE9BQU8sQ0FBQzRCLEdBQWYsSUFBc0IsUUFBMUIsRUFBb0M7QUFDekNTLGFBQUcsR0FBR3JDLE9BQU8sQ0FBQzRCLEdBQWQ7QUFDRCxTQUZNLE1BRUEsSUFBSSxPQUFPNUIsT0FBTyxDQUFDOEMsT0FBZixJQUEwQixRQUE5QixFQUF3QztBQUM3QyxlQUFLaEIsRUFBTCxHQUFVOUIsT0FBTyxDQUFDOEMsT0FBbEI7QUFDRDs7QUFDRCxZQUFJVCxHQUFKLEVBQVM7QUFDUCxjQUFNVSxDQUFDLEdBQUdWLEdBQUcsQ0FBQ1gsS0FBSixDQUFVLEtBQUtBLEtBQWYsQ0FBVjs7QUFDQSxjQUFJcUIsQ0FBSixFQUFPO0FBQ0wsaUJBQUtqQixFQUFMLEdBQVVpQixDQUFDLENBQUMsS0FBS3BCLFlBQU4sQ0FBWDtBQUNBM0IsbUJBQU8sQ0FBQzhDLE9BQVIsR0FBa0JDLENBQUMsQ0FBQyxLQUFLcEIsWUFBTixDQUFuQjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxVQUFJLEtBQUtHLEVBQVQsRUFBYTtBQUNYLFlBQUlNLE9BQU8sSUFBSUEsT0FBTyxDQUFDUSxPQUFSLElBQW1CLFFBQWxDLEVBQTRDO0FBQzFDLGVBQUs3QyxLQUFMLEdBQWFxQyxPQUFiO0FBQ0FBLGlCQUFPLENBQUNZLFlBQVIsQ0FBcUIsS0FBckIsRUFBNEIsS0FBS3BCLEdBQUwsQ0FBU25CLE9BQVQsQ0FBaUIsS0FBakIsRUFBd0IsS0FBS3FCLEVBQTdCLENBQTVCO0FBQ0QsU0FIRCxNQUdPLElBQUlNLE9BQUosRUFBYTtBQUNsQixlQUFLTCxTQUFMLEdBQWlCSyxPQUFqQjtBQUNEOztBQUNELFlBQUksQ0FBQyxLQUFLYSxLQUFOLElBQWUsT0FBT3pCLE1BQU0sQ0FBQyxLQUFLeUIsS0FBTixDQUFiLElBQTZCLFdBQTVDLElBQTJELENBQUN6QixNQUFNLENBQUMsS0FBS3lCLEtBQU4sQ0FBdEUsRUFBb0Y7QUFDbEYsY0FBSUMsTUFBTSxHQUFHUixRQUFRLENBQUNTLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjtBQUNBRCxnQkFBTSxDQUFDRSxJQUFQLEdBQWMsaUJBQWQ7QUFDQUYsZ0JBQU0sQ0FBQ2IsR0FBUCxHQUFhLEtBQUtSLEdBQWxCOztBQUNBcUIsZ0JBQU0sQ0FBQ0csTUFBUCxHQUFnQixZQUFNO0FBQ3BCLGlCQUFJLENBQUNwQixNQUFMLENBQVlDLE1BQVosR0FBcUIsSUFBckI7QUFDQSxtQkFBT2xDLE9BQU8sQ0FBQzRCLEdBQWY7O0FBQ0EsaUJBQUksQ0FBQzBCLFNBQUwsQ0FBZXRELE9BQWY7QUFDRCxXQUpEOztBQUtBMEMsa0JBQVEsQ0FBQ2EsSUFBVCxDQUFjQyxXQUFkLENBQTBCTixNQUExQjtBQUNELFNBVkQsTUFVTztBQUNMLGVBQUtqQixNQUFMLENBQVlDLE1BQVosR0FBcUIsSUFBckI7QUFDQSxpQkFBT2xDLE9BQU8sQ0FBQzRCLEdBQWY7QUFDQSxlQUFLMEIsU0FBTCxDQUFldEQsT0FBZjtBQUNEOztBQUNELGVBQU8sSUFBUDtBQUNEOztBQUNELGFBQU8sSUFBUDtBQUNEOzs7MkJBR00sQ0FDTjs7OzRCQUdPLENBQ1A7OzsyQkFHTSxDQUNOOzs7MkJBR00sQ0FDTjs7OzhCQUVTLENBQ1Q7Ozs2QkFHUSxDQUNSOzs7MkJBRU1pQixPLEVBQVMsQ0FDZjs7OzhCQUdTd0MsTSxFQUFRLENBQ2pCOzs7NEJBRU8vRCxDLEVBQUcyQixDLEVBQUcsQ0FDYjs7OzRCQUVPRSxJLEVBQU0sQ0FDYjs7OzhCQUdTdkIsTyxFQUFTLENBQ2xCOzs7Ozs7QUFHWXlCLG1FQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqSEE7O0lBRU01QixLOzs7Ozs7Ozs7Ozs7OzJCQUVHO0FBQ0wsV0FBSzZCLEtBQUwsR0FBYSxtQ0FBYjtBQUNBLFdBQUtDLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxXQUFLQyxHQUFMLEdBQVcsMENBQVg7QUFDQSxXQUFLckIsR0FBTCxHQUFXLDhMQUFYO0FBQ0EsV0FBS3NCLEdBQUwsR0FBVyx3Q0FBWDtBQUNBLFdBQUtvQixLQUFMLEdBQWEsd0NBQWI7QUFDQSxXQUFLbkIsRUFBTCxHQUFVLElBQVY7QUFDQSxXQUFLQyxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsV0FBS0MsTUFBTCxHQUFjLElBQWQ7QUFDQSxXQUFLMEIsT0FBTCxHQUFlLEdBQWY7QUFDQSxXQUFLQyxNQUFMLEdBQWMsQ0FBZDtBQUNBLFdBQUsxQixNQUFMLEdBQWM7QUFDWkMsY0FBTSxFQUFFO0FBREksT0FBZDtBQUdEOzs7OEJBRVNsQyxPLEVBQVM7QUFBQTs7QUFDakIsVUFBSSxLQUFLOEIsRUFBVCxFQUFhO0FBQ1gsWUFBSTRCLE9BQU8sR0FBR0UsV0FBVyxDQUFDLFlBQU07QUFFOUIsY0FBSSxLQUFJLENBQUMzQixNQUFMLENBQVlDLE1BQWhCLEVBQXdCO0FBQ3RCMkIseUJBQWEsQ0FBQ0gsT0FBRCxDQUFiOztBQUNBLGdCQUFJLEtBQUksQ0FBQzNELEtBQVQsRUFBZ0I7QUFDZCxtQkFBSSxDQUFDaUMsTUFBTCxHQUFjLElBQUk4QixLQUFLLENBQUNDLE1BQVYsQ0FBaUIsS0FBSSxDQUFDaEUsS0FBdEIsRUFBNkJDLE9BQTdCLENBQWQ7QUFDRCxhQUZELE1BRU8sSUFBSSxLQUFJLENBQUMrQixTQUFULEVBQW9CO0FBQ3pCL0IscUJBQU8sQ0FBQzhCLEVBQVIsR0FBYSxLQUFJLENBQUNBLEVBQWxCO0FBQ0EsbUJBQUksQ0FBQ0UsTUFBTCxHQUFjLElBQUk4QixLQUFLLENBQUNDLE1BQVYsQ0FBaUIsS0FBSSxDQUFDaEMsU0FBdEIsRUFBaUMvQixPQUFqQyxDQUFkO0FBQ0EsbUJBQUksQ0FBQ0QsS0FBTCxHQUFhLEtBQUksQ0FBQ2lDLE1BQUwsQ0FBWUksT0FBekI7QUFDRDtBQUVGO0FBQ0YsU0Fid0IsRUFhdEIsS0FBS3NCLE9BYmlCLENBQXpCO0FBY0Q7QUFDRjs7OzJCQUVNO0FBQUE7O0FBQ0wsVUFBSUEsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZdEIsSUFBbkIsSUFBMkIsVUFBOUMsRUFBMEQ7QUFDeEQsZ0JBQUksQ0FBQ3NCLE1BQUwsQ0FBWXRCLElBQVo7O0FBQ0FtRCx1QkFBYSxDQUFDSCxPQUFELENBQWI7QUFDRDtBQUNGLE9BTHdCLEVBS3RCLEtBQUtBLE9BTGlCLENBQXpCO0FBTUQ7Ozs4QkFFUztBQUNSLFVBQUksS0FBSzFCLE1BQUwsSUFBZSxPQUFPLEtBQUtBLE1BQUwsQ0FBWWpCLE9BQW5CLElBQThCLFVBQWpELEVBQTZEO0FBQzNELGFBQUtpQixNQUFMLENBQVlqQixPQUFaO0FBQ0Q7QUFDRjs7OzRCQUVPO0FBQUE7O0FBQ04sVUFBSTJDLE9BQU8sR0FBR0UsV0FBVyxDQUFDLFlBQU07QUFDOUIsWUFBSSxNQUFJLENBQUM1QixNQUFMLElBQWUsT0FBTyxNQUFJLENBQUNBLE1BQUwsQ0FBWXJCLEtBQW5CLElBQTRCLFVBQS9DLEVBQTJEO0FBQ3pELGdCQUFJLENBQUNxQixNQUFMLENBQVlyQixLQUFaOztBQUNBa0QsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQUx3QixFQUt0QixLQUFLQSxPQUxpQixDQUF6QjtBQU1EOzs7MkJBRU07QUFBQTs7QUFDTCxVQUFJQSxPQUFPLEdBQUdFLFdBQVcsQ0FBQyxZQUFNO0FBQzlCLFlBQUksTUFBSSxDQUFDNUIsTUFBTCxJQUFlLE9BQU8sTUFBSSxDQUFDQSxNQUFMLENBQVlyQixLQUFuQixJQUE0QixVQUEvQyxFQUEyRDtBQUN6RCxnQkFBSSxDQUFDcUIsTUFBTCxDQUFZckIsS0FBWjs7QUFDQSxnQkFBSSxDQUFDcUIsTUFBTCxDQUFZZ0MsY0FBWixDQUEyQixDQUEzQjs7QUFDQUgsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQU53QixFQU10QixLQUFLQSxPQU5pQixDQUF6QjtBQU9EOzs7MkJBRU07QUFBQTs7QUFDTCxVQUFJQSxPQUFPLEdBQUdFLFdBQVcsQ0FBQyxZQUFNO0FBQzlCLFlBQUksTUFBSSxDQUFDNUIsTUFBTCxJQUFlLE9BQU8sTUFBSSxDQUFDQSxNQUFMLENBQVlkLFNBQW5CLElBQWdDLFVBQW5ELEVBQStEO0FBQzdELGdCQUFJLENBQUNjLE1BQUwsQ0FBWWlDLFNBQVosR0FBd0JDLElBQXhCLENBQTZCLFVBQUNQLE1BQUQsRUFBWTtBQUN2QyxrQkFBSSxDQUFDQSxNQUFMLEdBQWNBLE1BQWQ7QUFDRCxXQUZEOztBQUdBLGdCQUFJLENBQUMzQixNQUFMLENBQVlkLFNBQVosQ0FBc0IsQ0FBdEI7O0FBQ0EyQyx1QkFBYSxDQUFDSCxPQUFELENBQWI7QUFDRDtBQUNGLE9BUndCLEVBUXRCLEtBQUtBLE9BUmlCLENBQXpCO0FBU0Q7Ozs2QkFFUTtBQUFBOztBQUNQLFVBQUlBLE9BQU8sR0FBR0UsV0FBVyxDQUFDLFlBQU07QUFDOUIsWUFBSSxNQUFJLENBQUM1QixNQUFMLElBQWUsT0FBTyxNQUFJLENBQUNBLE1BQUwsQ0FBWWQsU0FBbkIsSUFBZ0MsVUFBbkQsRUFBK0Q7QUFDN0QsY0FBSSxNQUFJLENBQUN5QyxNQUFMLElBQWUsQ0FBbkIsRUFBc0I7QUFDcEIsa0JBQUksQ0FBQ0EsTUFBTCxHQUFjLENBQWQ7QUFDRDs7QUFDRCxnQkFBSSxDQUFDM0IsTUFBTCxDQUFZZCxTQUFaLENBQXNCLE1BQUksQ0FBQ3lDLE1BQTNCOztBQUNBRSx1QkFBYSxDQUFDSCxPQUFELENBQWI7QUFDRDtBQUNGLE9BUndCLEVBUXRCLEtBQUtBLE9BUmlCLENBQXpCO0FBU0Q7OzsyQkFFTXpDLE8sRUFBUztBQUFBOztBQUNkLFVBQUl5QyxPQUFPLEdBQUdFLFdBQVcsQ0FBQyxZQUFNO0FBQzlCLFlBQUksTUFBSSxDQUFDNUIsTUFBTCxJQUFlLE9BQU8sTUFBSSxDQUFDQSxNQUFMLENBQVlnQyxjQUFuQixJQUFxQyxVQUF4RCxFQUFvRTtBQUNsRSxnQkFBSSxDQUFDaEMsTUFBTCxDQUFZZ0MsY0FBWixDQUEyQi9DLE9BQTNCOztBQUNBNEMsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQUx3QixFQUt0QixLQUFLQSxPQUxpQixDQUF6QjtBQU1EOzs7OEJBRVNELE0sRUFBUTtBQUFBOztBQUNoQixVQUFJQyxPQUFPLEdBQUdFLFdBQVcsQ0FBQyxZQUFNO0FBQzlCLFlBQUksTUFBSSxDQUFDNUIsTUFBTCxJQUFlLE9BQU8sTUFBSSxDQUFDQSxNQUFMLENBQVlkLFNBQW5CLElBQWdDLFVBQW5ELEVBQStEO0FBQzdELGNBQU15QyxNQUFNLEdBQUdGLE1BQU0sR0FBRyxHQUFULEdBQWUsQ0FBZixHQUFtQkEsTUFBTSxHQUFHLEdBQTNDOztBQUVBLGdCQUFJLENBQUN6QixNQUFMLENBQVlkLFNBQVosQ0FBc0J5QyxNQUF0Qjs7QUFDQUUsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQVB3QixFQU90QixLQUFLQSxPQVBpQixDQUF6QjtBQVFEOzs7NEJBRVFuQyxJLEVBQU87QUFBQTs7QUFDZCxVQUFJbUMsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZVixPQUFuQixJQUE4QixVQUFqRCxFQUE2RDtBQUMzRCxnQkFBSSxDQUFDVSxNQUFMLENBQVlWLE9BQVosQ0FBcUJDLElBQXJCOztBQUNBc0MsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQUx3QixFQUt0QixLQUFLQSxPQUxpQixDQUF6QjtBQU1EOzs7NEJBRU9oRSxDLEVBQUcyQixDLEVBQUc7QUFBQTs7QUFDWixVQUFJcUMsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE9BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE9BQUksQ0FBQ0EsTUFBTCxDQUFZSSxPQUFuQixJQUE4QixXQUFqRCxFQUE4RDtBQUM1RCxpQkFBSSxDQUFDSixNQUFMLENBQVlJLE9BQVosQ0FBb0JZLFlBQXBCLENBQWlDLE9BQWpDLEVBQTBDdEQsQ0FBMUM7O0FBQ0EsaUJBQUksQ0FBQ3NDLE1BQUwsQ0FBWUksT0FBWixDQUFvQlksWUFBcEIsQ0FBaUMsUUFBakMsRUFBMkMzQixDQUEzQzs7QUFDQXdDLHVCQUFhLENBQUNILE9BQUQsQ0FBYjtBQUNEO0FBQ0YsT0FOd0IsRUFNdEIsS0FBS0EsT0FOaUIsQ0FBekI7QUFPRDs7OztFQXJJaUJqQyw2Qzs7QUF3SUw1QixvRUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUlBOztJQUVNRCxPOzs7Ozs7Ozs7Ozs7OzJCQUNHO0FBQ0wsV0FBSzhCLEtBQUwsR0FBYSx1SkFBYjtBQUNBLFdBQUtDLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxXQUFLQyxHQUFMLEdBQVcsMERBQVg7QUFDQSxXQUFLckIsR0FBTCxHQUFXLDhMQUFYO0FBQ0EsV0FBS3NCLEdBQUwsR0FBVyxvQ0FBWDtBQUNBLFdBQUtDLEVBQUwsR0FBVSxJQUFWO0FBQ0EsV0FBS0MsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUtDLE1BQUwsR0FBYyxJQUFkO0FBQ0EsV0FBSzBCLE9BQUwsR0FBZSxHQUFmO0FBQ0EsV0FBS1MsSUFBTCxHQUFZLENBQVo7QUFDQSxXQUFLbEMsTUFBTCxHQUFjO0FBQ1pDLGNBQU0sRUFBRTtBQURJLE9BQWQ7QUFHRDs7O3lCQUVJQyxNLEVBQVE7QUFBQTs7QUFDWCxVQUFJQyxPQUFKO0FBQ0EsVUFBSUMsR0FBSjtBQUNBLFVBQUlyQyxPQUFPLEdBQUdzQyxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCSixNQUFNLENBQUNuQyxPQUF6QixDQUFkOztBQUNBLFVBQUltQyxNQUFNLENBQUNwQyxLQUFQLFlBQXdCeUMsT0FBeEIsSUFBbUNMLE1BQU0sQ0FBQ3BDLEtBQVAsWUFBd0IwQyxZQUEvRCxFQUE2RTtBQUMzRUwsZUFBTyxHQUFHRCxNQUFNLENBQUNwQyxLQUFqQjtBQUNELE9BRkQsTUFFTyxJQUFJLE9BQU9vQyxNQUFNLENBQUNwQyxLQUFkLElBQXVCLFFBQTNCLEVBQXFDO0FBQzFDcUMsZUFBTyxHQUFHTSxRQUFRLENBQUNDLGFBQVQsQ0FBdUJSLE1BQU0sQ0FBQ3BDLEtBQTlCLENBQVY7QUFDRDs7QUFDRCxVQUFJcUMsT0FBSixFQUFhO0FBQ1gsWUFBSUEsT0FBTyxDQUFDUSxPQUFSLElBQW1CLFFBQXZCLEVBQWlDO0FBQy9CUCxhQUFHLEdBQUdELE9BQU8sQ0FBQ1MsWUFBUixDQUFxQixLQUFyQixDQUFOO0FBQ0QsU0FGRCxNQUVPLElBQUksT0FBTzdDLE9BQU8sQ0FBQzRCLEdBQWYsSUFBc0IsUUFBMUIsRUFBb0M7QUFDekNTLGFBQUcsR0FBR3JDLE9BQU8sQ0FBQzRCLEdBQWQ7QUFDRCxTQUZNLE1BRUEsSUFBSSxPQUFPNUIsT0FBTyxDQUFDOEMsT0FBZixJQUEwQixRQUE5QixFQUF3QztBQUM3QyxlQUFLaEIsRUFBTCxHQUFVOUIsT0FBTyxDQUFDOEMsT0FBbEI7QUFDRDs7QUFDRCxZQUFJVCxHQUFKLEVBQVM7QUFDUCxjQUFNVSxDQUFDLEdBQUdWLEdBQUcsQ0FBQ1gsS0FBSixDQUFVLEtBQUtBLEtBQWYsQ0FBVjs7QUFDQSxjQUFJcUIsQ0FBSixFQUFPO0FBQ0wsaUJBQUtqQixFQUFMLEdBQVVpQixDQUFDLENBQUMsS0FBS3BCLFlBQU4sQ0FBWDtBQUNBM0IsbUJBQU8sQ0FBQzhDLE9BQVIsR0FBa0JDLENBQUMsQ0FBQyxLQUFLcEIsWUFBTixDQUFuQjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxVQUFJLEtBQUtHLEVBQVQsRUFBYTtBQUNYLFlBQUlNLE9BQU8sSUFBSUEsT0FBTyxDQUFDUSxPQUFSLElBQW1CLFFBQWxDLEVBQTRDO0FBQzFDLGVBQUs3QyxLQUFMLEdBQWFxQyxPQUFiO0FBQ0FBLGlCQUFPLENBQUNZLFlBQVIsQ0FBcUIsS0FBckIsRUFBNEIsS0FBS3BCLEdBQUwsQ0FBU25CLE9BQVQsQ0FBaUIsS0FBakIsRUFBd0IsS0FBS3FCLEVBQTdCLENBQTVCO0FBQ0QsU0FIRCxNQUdPLElBQUlNLE9BQUosRUFBYTtBQUNsQixlQUFLTCxTQUFMLEdBQWlCSyxPQUFqQjtBQUNEOztBQUNELFlBQUksT0FBT2dDLEVBQVAsSUFBYSxXQUFiLElBQTRCLENBQUNBLEVBQWpDLEVBQXFDO0FBQ25DLGNBQUlsQixNQUFNLEdBQUdSLFFBQVEsQ0FBQ1MsYUFBVCxDQUF1QixRQUF2QixDQUFiO0FBQ0FELGdCQUFNLENBQUNFLElBQVAsR0FBYyxpQkFBZDtBQUNBRixnQkFBTSxDQUFDYixHQUFQLEdBQWEsS0FBS1IsR0FBbEI7O0FBQ0FMLGdCQUFNLENBQUM2Qyx1QkFBUCxHQUFpQyxZQUFNO0FBQ3JDLGlCQUFJLENBQUNwQyxNQUFMLENBQVlDLE1BQVosR0FBcUIsSUFBckI7QUFDQSxtQkFBT2xDLE9BQU8sQ0FBQzRCLEdBQWY7O0FBQ0EsaUJBQUksQ0FBQzBCLFNBQUwsQ0FBZXRELE9BQWY7QUFDRCxXQUpEOztBQUtBMEMsa0JBQVEsQ0FBQ2EsSUFBVCxDQUFjQyxXQUFkLENBQTBCTixNQUExQjtBQUNELFNBVkQsTUFVTztBQUNMLGVBQUtqQixNQUFMLENBQVlDLE1BQVosR0FBcUIsSUFBckI7QUFDQSxpQkFBT2xDLE9BQU8sQ0FBQzRCLEdBQWY7QUFDQSxlQUFLMEIsU0FBTCxDQUFldEQsT0FBZjtBQUNEOztBQUNELGVBQU8sSUFBUDtBQUNEOztBQUNELGFBQU8sSUFBUDtBQUNEOzs7MkJBRU07QUFBQTs7QUFDTCxVQUFJMEQsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZc0MsU0FBbkIsSUFBZ0MsVUFBbkQsRUFBK0Q7QUFDN0QsZ0JBQUksQ0FBQ3RDLE1BQUwsQ0FBWXNDLFNBQVo7O0FBQ0FULHVCQUFhLENBQUNILE9BQUQsQ0FBYjtBQUNEO0FBQ0YsT0FMd0IsRUFLdEIsS0FBS0EsT0FMaUIsQ0FBekI7QUFNRDs7OzRCQUVPO0FBQUE7O0FBQ04sVUFBSUEsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZdUMsVUFBbkIsSUFBaUMsVUFBcEQsRUFBZ0U7QUFDOUQsZ0JBQUksQ0FBQ3ZDLE1BQUwsQ0FBWXVDLFVBQVo7O0FBQ0FWLHVCQUFhLENBQUNILE9BQUQsQ0FBYjtBQUNEO0FBQ0YsT0FMd0IsRUFLdEIsS0FBS0EsT0FMaUIsQ0FBekI7QUFNRDs7OzJCQUVNO0FBQUE7O0FBQ0wsVUFBSUEsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZd0MsU0FBbkIsSUFBZ0MsVUFBbkQsRUFBK0Q7QUFDN0QsZ0JBQUksQ0FBQ3hDLE1BQUwsQ0FBWXdDLFNBQVo7O0FBQ0FYLHVCQUFhLENBQUNILE9BQUQsQ0FBYjtBQUNEO0FBQ0YsT0FMd0IsRUFLdEIsS0FBS0EsT0FMaUIsQ0FBekI7QUFNRDs7OzhCQUVTO0FBQ1IsVUFBSSxLQUFLMUIsTUFBTCxJQUFlLE9BQU8sS0FBS0EsTUFBTCxDQUFZakIsT0FBbkIsSUFBOEIsVUFBakQsRUFBNkQ7QUFDM0QsYUFBS2lCLE1BQUwsQ0FBWWpCLE9BQVo7QUFDRDtBQUNGOzs7MkJBRU07QUFBQTs7QUFDTCxVQUFJMkMsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZbkIsSUFBbkIsSUFBMkIsVUFBOUMsRUFBMEQ7QUFDeEQsZ0JBQUksQ0FBQ21CLE1BQUwsQ0FBWW5CLElBQVo7O0FBQ0FnRCx1QkFBYSxDQUFDSCxPQUFELENBQWI7QUFDRDtBQUNGLE9BTHdCLEVBS3RCLEtBQUtBLE9BTGlCLENBQXpCO0FBTUQ7Ozs2QkFFUTtBQUFBOztBQUNQLFVBQUlBLE9BQU8sR0FBR0UsV0FBVyxDQUFDLFlBQU07QUFDOUIsWUFBSSxNQUFJLENBQUM1QixNQUFMLElBQWUsT0FBTyxNQUFJLENBQUNBLE1BQUwsQ0FBWWxCLE1BQW5CLElBQTZCLFVBQWhELEVBQTREO0FBQzFELGdCQUFJLENBQUNrQixNQUFMLENBQVlsQixNQUFaOztBQUNBK0MsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQUx3QixFQUt0QixLQUFLQSxPQUxpQixDQUF6QjtBQU1EOzs7MkJBRU16QyxPLEVBQVM7QUFBQTs7QUFDZCxVQUFJeUMsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZaEIsTUFBbkIsSUFBNkIsVUFBaEQsRUFBNEQ7QUFDMUQsZ0JBQUksQ0FBQ2dCLE1BQUwsQ0FBWWhCLE1BQVosQ0FBbUJDLE9BQW5COztBQUNBNEMsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQUx3QixFQUt0QkEsT0FMc0IsQ0FBekI7QUFNRDs7OzhCQUVTRCxNLEVBQVE7QUFBQTs7QUFDaEIsVUFBSUMsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZZCxTQUFuQixJQUFnQyxVQUFuRCxFQUErRDtBQUM3RCxnQkFBSSxDQUFDYyxNQUFMLENBQVlkLFNBQVosQ0FBc0J1QyxNQUF0Qjs7QUFDQUksdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQUx3QixFQUt0QixLQUFLQSxPQUxpQixDQUF6QjtBQU1EOzs7NEJBRU9oRSxDLEVBQUcyQixDLEVBQUc7QUFBQTs7QUFDWixVQUFJcUMsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE1BQUksQ0FBQzVCLE1BQUwsSUFBZSxPQUFPLE1BQUksQ0FBQ0EsTUFBTCxDQUFZWixPQUFuQixJQUE4QixVQUFqRCxFQUE2RDtBQUMzRCxnQkFBSSxDQUFDWSxNQUFMLENBQVl5QyxDQUFaLENBQWN6QixZQUFkLENBQTJCLE9BQTNCLEVBQW9DdEQsQ0FBcEM7O0FBQ0EsZ0JBQUksQ0FBQ3NDLE1BQUwsQ0FBWXlDLENBQVosQ0FBY3pCLFlBQWQsQ0FBMkIsUUFBM0IsRUFBcUMzQixDQUFyQzs7QUFDQXdDLHVCQUFhLENBQUNILE9BQUQsQ0FBYjtBQUNEO0FBQ0YsT0FOd0IsRUFNdEIsS0FBS0EsT0FOaUIsQ0FBekI7QUFPRDs7OzhCQUVTMUQsTyxFQUFTO0FBQUE7O0FBQ2pCLFVBQUksS0FBSzhCLEVBQVQsRUFBYTtBQUNYOUIsZUFBTyxDQUFDMEUsTUFBUixHQUFpQjtBQUNmLDJCQUFpQix1QkFBQ0MsQ0FBRCxFQUFPO0FBQ3RCLGdCQUFJUCxFQUFFLENBQUNRLFdBQUgsQ0FBZUMsS0FBZixJQUF3QkYsQ0FBQyxDQUFDRyxJQUE5QixFQUFvQztBQUNsQyxxQkFBSSxDQUFDQyxNQUFMO0FBQ0Q7QUFDRjtBQUxjLFNBQWpCO0FBT0EsWUFBSXJCLE9BQU8sR0FBR0UsV0FBVyxDQUFDLFlBQU07QUFDOUIsY0FBSSxPQUFJLENBQUMzQixNQUFMLENBQVlDLE1BQWhCLEVBQXdCO0FBQ3RCMkIseUJBQWEsQ0FBQ0gsT0FBRCxDQUFiOztBQUNBLGdCQUFJLE9BQUksQ0FBQzNELEtBQVQsRUFBZ0I7QUFDZCxxQkFBSSxDQUFDaUMsTUFBTCxHQUFjLElBQUlvQyxFQUFFLENBQUNMLE1BQVAsQ0FBYyxPQUFJLENBQUNoRSxLQUFuQixFQUEwQkMsT0FBMUIsQ0FBZDtBQUNELGFBRkQsTUFFTyxJQUFJLE9BQUksQ0FBQytCLFNBQVQsRUFBb0I7QUFDekIscUJBQUksQ0FBQ0MsTUFBTCxHQUFjLElBQUlvQyxFQUFFLENBQUNMLE1BQVAsQ0FBYyxPQUFJLENBQUNoQyxTQUFuQixFQUE4Qi9CLE9BQTlCLENBQWQ7QUFDQSxxQkFBSSxDQUFDRCxLQUFMLEdBQWEsT0FBSSxDQUFDaUMsTUFBTCxDQUFZeUMsQ0FBekI7QUFDRDtBQUVGO0FBQ0YsU0FYd0IsRUFXdEIsS0FBS2YsT0FYaUIsQ0FBekI7QUFZRDtBQUNGOzs7NEJBRU9uQyxJLEVBQU07QUFBQTs7QUFDWixVQUFJbUMsT0FBTyxHQUFHRSxXQUFXLENBQUMsWUFBTTtBQUM5QixZQUFJLE9BQUksQ0FBQzVCLE1BQVQsRUFBaUI7QUFDZixpQkFBSSxDQUFDbUMsSUFBTCxHQUFZNUMsSUFBWjtBQUNBc0MsdUJBQWEsQ0FBQ0gsT0FBRCxDQUFiO0FBQ0Q7QUFDRixPQUx3QixFQUt0QixLQUFLQSxPQUxpQixDQUF6QjtBQU1EOzs7NkJBRVE7QUFDUCxVQUFJLEtBQUsxQixNQUFMLElBQWUsS0FBS21DLElBQXhCLEVBQThCO0FBQzVCLGFBQUtuQyxNQUFMLENBQVloQixNQUFaLENBQW1CLENBQW5CO0FBQ0Q7QUFDRjs7OztFQTFMbUJTLDZDOztBQTZMUDdCLHNFQUFmLEUiLCJmaWxlIjoianMvbmF2aWRlb2FwaS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2Rldi9uYXZpZGVvYXBpLmpzXCIpO1xuIiwiaW1wb3J0IHlvdXR1YmUgZnJvbSBcIi4vcHJvdmlkZXJzL3lvdXR1YmVcIjtcclxuaW1wb3J0IHZpbWVvIGZyb20gXCIuL3Byb3ZpZGVycy92aW1lb1wiO1xyXG5cclxuKCh3KSA9PiB7XHJcbiAgY29uc3QgbmFWaWRlb0FwaURhdGEgPSB7XHJcbiAgICB5b3V0dWJlLFxyXG4gICAgdmltZW9cclxuICB9XHJcblxyXG4gIHcubmFWaWRlb0FwaSA9IGZ1bmN0aW9uICh2aWRlbywgb3B0aW9ucykge1xyXG4gICAgaWYgKHZpZGVvKSB7XHJcbiAgICAgIHRoaXMudmlkZW8gPSB2aWRlb1xyXG4gICAgICB0aGlzLm9wdGlvbnMgPSBvcHRpb25zO1xyXG4gICAgICB0aGlzLmluaXQoKTtcclxuICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9XHJcbiAgbmFWaWRlb0FwaS5wcm90b3R5cGUgPSB7XHJcblxyXG4gICAgcHJvdmlkZXI6IG51bGwsXHJcbiAgICAvKipcclxuICAgICAqIEluaXQgcHJvdmlkZXJcclxuICAgICAqL1xyXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICBmb3IgKHZhciBwcm92aWRlck5hbWUgaW4gbmFWaWRlb0FwaURhdGEpIHtcclxuICAgICAgICBjb25zdCBwcm92aWRlciA9IG5ldyBuYVZpZGVvQXBpRGF0YVtwcm92aWRlck5hbWVdKCk7XHJcbiAgICAgICAgaWYgKHByb3ZpZGVyLmxvYWQoIHRoaXMgKSkge1xyXG4gICAgICAgICAgdGhpcy5wcm92aWRlciA9IHByb3ZpZGVyO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICAvKipcclxuICAgICAqIEdldCBodG1sIHZpZGVvXHJcbiAgICAgKi9cclxuICAgIGdldFRwbDogZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAodGhpcy5wcm92aWRlciAmJiB0aGlzLnByb3ZpZGVyLnRwbCAmJiB0aGlzLmVtYmVkZWRVcmwpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm92aWRlci50cGwucmVwbGFjZSgnJHVybCcsIHRoaXMuZW1iZWRlZFVybCk7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICAvKipcclxuICAgICAqIFZpZGVvIGNvbnRyb2xzXHJcbiAgICAgKi9cclxuICAgIHBsYXk6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKHRoaXMucHJvdmlkZXIpIHtcclxuXHJcbiAgICAgICAgdGhpcy5wcm92aWRlci5wbGF5KCk7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBwYXVzZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAodGhpcy5wcm92aWRlcikge1xyXG4gICAgICAgIHRoaXMucHJvdmlkZXIucGF1c2UoKTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHN0b3A6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKHRoaXMucHJvdmlkZXIpIHtcclxuICAgICAgICB0aGlzLnByb3ZpZGVyLnN0b3AoKTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIG11dGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKHRoaXMucHJvdmlkZXIpIHtcclxuICAgICAgICB0aGlzLnByb3ZpZGVyLm11dGUoKTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHVuTXV0ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAodGhpcy5wcm92aWRlcikge1xyXG4gICAgICAgIHRoaXMucHJvdmlkZXIudW5NdXRlKCk7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkZXN0cm95OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGlmICh0aGlzLnByb3ZpZGVyKSB7XHJcbiAgICAgICAgdGhpcy5wcm92aWRlci5kZXN0cm95KCk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5vcHRpb25zID0gbnVsbDtcclxuICAgIH0sXHJcbiAgICBzZWVrVG86IGZ1bmN0aW9uIChzZWNvbmRzKSB7XHJcbiAgICAgIGlmICghc2Vjb25kcykge1xyXG4gICAgICAgIHNlY29uZHMgPSAwO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLnByb3ZpZGVyKSB7XHJcbiAgICAgICAgdGhpcy5wcm92aWRlci5zZWVrVG8oc2Vjb25kcyk7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBzZXRWb2x1bWU6IGZ1bmN0aW9uIChsZXZlbCkge1xyXG4gICAgICBpZiAoIWxldmVsKSB7XHJcbiAgICAgICAgbGV2ZWwgPSAxO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLnByb3ZpZGVyKSB7XHJcbiAgICAgICAgdGhpcy5wcm92aWRlci5zZXRWb2x1bWUobGV2ZWwpO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgc2V0U2l6ZTogZnVuY3Rpb24gKHcsIGgpIHtcclxuICAgICAgaWYgKCF3KSB7XHJcbiAgICAgICAgdyA9IDM0MDtcclxuICAgICAgfVxyXG4gICAgICBpZiAoIWgpIHtcclxuICAgICAgICBoID0gNDgwO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLnByb3ZpZGVyKSB7XHJcbiAgICAgICAgdGhpcy5wcm92aWRlci5zZXRTaXplKHcsIGgpO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgc2V0TG9vcDogZnVuY3Rpb24gKCBib29sID0gdHJ1ZSApIHtcclxuICAgICAgaWYgKHRoaXMucHJvdmlkZXIpIHtcclxuICAgICAgICB0aGlzLnByb3ZpZGVyLnNldExvb3AoIGJvb2wgKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICB9XHJcbn0pKHdpbmRvdylcclxuXHJcbmV4cG9ydCBkZWZhdWx0IG5hVmlkZW9BcGkiLCJjbGFzcyBiYXNlIHtcclxuICBpbml0KCkge1xyXG4gICAgdGhpcy5tYXRjaCA9IC9eKFtcXGRdKyk/LztcclxuICAgIHRoaXMuaWRNYXRjaEluZGV4ID0gMTtcclxuICAgIHRoaXMudXJsID0gXCJodHRwczovL3d3dy5leGFtcGxlLmNvbS8kaWQ/YXBpPTFcIjtcclxuICAgIHRoaXMudHBsID0gXCI8aWZyYW1lICBzcmM9XFxcIiR1cmxcXFwiIGZyYW1lYm9yZGVyPVxcXCIwXFxcIiBhbGxvdz1cXFwiYWNjZWxlcm9tZXRlcjsgYXV0b3BsYXk7IGVuY3J5cHRlZC1tZWRpYTsgZ3lyb3Njb3BlOyBwaWN0dXJlLWluLXBpY3R1cmVcXFwiIHdlYmtpdGFsbG93ZnVsbHNjcmVlbiBtb3phbGxvd2Z1bGxzY3JlZW4gYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPlwiO1xyXG4gICAgdGhpcy5hcGkgPSBcImh0dHBzOi8vd3d3LmV4YW1wbGUuY29tL2FwaVwiO1xyXG4gICAgdGhpcy5pZCA9IG51bGw7XHJcbiAgICB0aGlzLmNvbnRhaW5lciA9IG51bGw7XHJcbiAgICB0aGlzLnBsYXllciA9IG51bGw7XHJcbiAgICB0aGlzLnN0YXR1cyA9IHtcclxuICAgICAgbG9hZGVkOiBmYWxzZSxcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMuaW5pdCgpO1xyXG4gIH1cclxuXHJcbiAgbG9hZChwbHVnaW4pIHtcclxuICAgIGxldCBlbGVtZW50O1xyXG4gICAgbGV0IHNyYztcclxuICAgIGxldCBvcHRpb25zID0gIE9iamVjdC5hc3NpZ24oe30sIHBsdWdpbi5vcHRpb25zKTtcclxuICAgIGlmIChwbHVnaW4udmlkZW8gaW5zdGFuY2VvZiBFbGVtZW50IHx8IHBsdWdpbi52aWRlbyBpbnN0YW5jZW9mIEhUTUxEb2N1bWVudCkge1xyXG4gICAgICBlbGVtZW50ID0gcGx1Z2luLnZpZGVvO1xyXG4gICAgfSBlbHNlIGlmICh0eXBlb2YgcGx1Z2luLnZpZGVvID09IFwic3RyaW5nXCIpIHtcclxuICAgICAgZWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IocGx1Z2luLnZpZGVvKTtcclxuICAgIH1cclxuICAgIGlmIChlbGVtZW50KSB7XHJcbiAgICAgIGlmIChlbGVtZW50LnRhZ05hbWUgPT0gJ0lGUkFNRScpIHtcclxuICAgICAgICBzcmMgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnc3JjJyk7XHJcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIG9wdGlvbnMudXJsID09IFwic3RyaW5nXCIpIHtcclxuICAgICAgICBzcmMgPSBvcHRpb25zLnVybDtcclxuICAgICAgfSBlbHNlIGlmICh0eXBlb2Ygb3B0aW9ucy52aWRlb0lkID09IFwic3RyaW5nXCIpIHtcclxuICAgICAgICB0aGlzLmlkID0gb3B0aW9ucy52aWRlb0lkO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChzcmMpIHtcclxuICAgICAgICBjb25zdCByID0gc3JjLm1hdGNoKHRoaXMubWF0Y2gpXHJcbiAgICAgICAgaWYgKHIpIHtcclxuICAgICAgICAgIHRoaXMuaWQgPSByW3RoaXMuaWRNYXRjaEluZGV4XTtcclxuICAgICAgICAgIG9wdGlvbnMudmlkZW9JZCA9IHJbdGhpcy5pZE1hdGNoSW5kZXhdO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmlkKSB7XHJcbiAgICAgIGlmIChlbGVtZW50ICYmIGVsZW1lbnQudGFnTmFtZSA9PSAnSUZSQU1FJykge1xyXG4gICAgICAgIHRoaXMudmlkZW8gPSBlbGVtZW50O1xyXG4gICAgICAgIGVsZW1lbnQuc2V0QXR0cmlidXRlKCdzcmMnLCB0aGlzLnVybC5yZXBsYWNlKCckaWQnLCB0aGlzLmlkKSk7XHJcbiAgICAgIH0gZWxzZSBpZiAoZWxlbWVudCkge1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0gZWxlbWVudDtcclxuICAgICAgfVxyXG4gICAgICBpZiAoIXRoaXMuY2xhc3MgfHwgdHlwZW9mIHdpbmRvd1t0aGlzLmNsYXNzXSA9PSBcInVuZGVmaW5lZFwiIHx8ICF3aW5kb3dbdGhpcy5jbGFzc10pIHtcclxuICAgICAgICBsZXQgc2NyaXB0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInNjcmlwdFwiKTtcclxuICAgICAgICBzY3JpcHQudHlwZSA9IFwidGV4dC9qYXZhc2NyaXB0XCI7XHJcbiAgICAgICAgc2NyaXB0LnNyYyA9IHRoaXMuYXBpO1xyXG4gICAgICAgIHNjcmlwdC5vbmxvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnN0YXR1cy5sb2FkZWQgPSB0cnVlO1xyXG4gICAgICAgICAgZGVsZXRlIG9wdGlvbnMudXJsO1xyXG4gICAgICAgICAgdGhpcy5nZXRQbGF5ZXIob3B0aW9ucyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoc2NyaXB0KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc3RhdHVzLmxvYWRlZCA9IHRydWU7XHJcbiAgICAgICAgZGVsZXRlIG9wdGlvbnMudXJsO1xyXG4gICAgICAgIHRoaXMuZ2V0UGxheWVyKG9wdGlvbnMpO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG51bGw7XHJcbiAgfVxyXG5cclxuXHJcbiAgcGxheSgpIHtcclxuICB9XHJcblxyXG5cclxuICBwYXVzZSgpIHtcclxuICB9XHJcblxyXG5cclxuICBzdG9wKCkge1xyXG4gIH1cclxuXHJcblxyXG4gIG11dGUoKSB7XHJcbiAgfVxyXG5cclxuICBkZXN0cm95KCkge1xyXG4gIH1cclxuXHJcblxyXG4gIHVuTXV0ZSgpIHtcclxuICB9XHJcblxyXG4gIHNlZWtUbyhzZWNvbmRzKSB7XHJcbiAgfVxyXG5cclxuXHJcbiAgc2V0Vm9sdW1lKG51bWJlcikge1xyXG4gIH1cclxuXHJcbiAgc2V0U2l6ZSh3LCBoKSB7XHJcbiAgfVxyXG5cclxuICBzZXRMb29wKGJvb2wpIHtcclxuICB9XHJcblxyXG5cclxuICBnZXRQbGF5ZXIob3B0aW9ucykge1xyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgYmFzZSIsImltcG9ydCBiYXNlIGZyb20gJy4vYmFzZSc7XHJcblxyXG5jbGFzcyB2aW1lbyBleHRlbmRzIGJhc2Uge1xyXG5cclxuICBpbml0KCkge1xyXG4gICAgdGhpcy5tYXRjaCA9IC9eLit2aW1lby5jb21cXC8oLipcXC8pPyhbXFxkXSspKC4qKT8vO1xyXG4gICAgdGhpcy5pZE1hdGNoSW5kZXggPSAyO1xyXG4gICAgdGhpcy51cmwgPSBcImh0dHBzOi8vcGxheWVyLnZpbWVvLmNvbS92aWRlby8kaWQ/YXBpPTFcIjtcclxuICAgIHRoaXMudHBsID0gXCI8aWZyYW1lICBzcmM9XFxcIiR1cmxcXFwiIGZyYW1lYm9yZGVyPVxcXCIwXFxcIiBhbGxvdz1cXFwiYWNjZWxlcm9tZXRlcjsgYXV0b3BsYXk7IGVuY3J5cHRlZC1tZWRpYTsgZ3lyb3Njb3BlOyBwaWN0dXJlLWluLXBpY3R1cmVcXFwiIHdlYmtpdGFsbG93ZnVsbHNjcmVlbiBtb3phbGxvd2Z1bGxzY3JlZW4gYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPlwiO1xyXG4gICAgdGhpcy5hcGkgPSBcImh0dHBzOi8vcGxheWVyLnZpbWVvLmNvbS9hcGkvcGxheWVyLmpzXCI7XHJcbiAgICB0aGlzLmNsYXNzID0gXCJodHRwczovL3BsYXllci52aW1lby5jb20vYXBpL3BsYXllci5qc1wiO1xyXG4gICAgdGhpcy5pZCA9IG51bGw7XHJcbiAgICB0aGlzLmNvbnRhaW5lciA9IG51bGw7XHJcbiAgICB0aGlzLnBsYXllciA9IG51bGw7XHJcbiAgICB0aGlzLnRpbWVvdXQgPSAxMDA7XHJcbiAgICB0aGlzLnZvbHVtZSA9IDA7XHJcbiAgICB0aGlzLnN0YXR1cyA9IHtcclxuICAgICAgbG9hZGVkOiBmYWxzZVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGdldFBsYXllcihvcHRpb25zKSB7XHJcbiAgICBpZiAodGhpcy5pZCkge1xyXG4gICAgICBsZXQgdGltZW91dCA9IHNldEludGVydmFsKCgpID0+IHtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdHVzLmxvYWRlZCkge1xyXG4gICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICAgICAgaWYgKHRoaXMudmlkZW8pIHtcclxuICAgICAgICAgICAgdGhpcy5wbGF5ZXIgPSBuZXcgVmltZW8uUGxheWVyKHRoaXMudmlkZW8sIG9wdGlvbnMpO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmNvbnRhaW5lcikge1xyXG4gICAgICAgICAgICBvcHRpb25zLmlkID0gdGhpcy5pZDtcclxuICAgICAgICAgICAgdGhpcy5wbGF5ZXIgPSBuZXcgVmltZW8uUGxheWVyKHRoaXMuY29udGFpbmVyLCBvcHRpb25zKTtcclxuICAgICAgICAgICAgdGhpcy52aWRlbyA9IHRoaXMucGxheWVyLmVsZW1lbnQ7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcGxheSgpIHtcclxuICAgIGxldCB0aW1lb3V0ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5wbGF5ZXIgJiYgdHlwZW9mIHRoaXMucGxheWVyLnBsYXkgPT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIucGxheSgpO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgZGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuZGVzdHJveSA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgdGhpcy5wbGF5ZXIuZGVzdHJveSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcGF1c2UoKSB7XHJcbiAgICBsZXQgdGltZW91dCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgaWYgKHRoaXMucGxheWVyICYmIHR5cGVvZiB0aGlzLnBsYXllci5wYXVzZSA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICB0aGlzLnBsYXllci5wYXVzZSgpO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgc3RvcCgpIHtcclxuICAgIGxldCB0aW1lb3V0ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5wbGF5ZXIgJiYgdHlwZW9mIHRoaXMucGxheWVyLnBhdXNlID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMucGxheWVyLnBhdXNlKCk7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuc2V0Q3VycmVudFRpbWUoMCk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICB9XHJcbiAgICB9LCB0aGlzLnRpbWVvdXQpXHJcbiAgfVxyXG5cclxuICBtdXRlKCkge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuc2V0Vm9sdW1lID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMucGxheWVyLmdldFZvbHVtZSgpLnRoZW4oKHZvbHVtZSkgPT4ge1xyXG4gICAgICAgICAgdGhpcy52b2x1bWUgPSB2b2x1bWU7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuc2V0Vm9sdW1lKDApO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgdW5NdXRlKCkge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuc2V0Vm9sdW1lID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIGlmICh0aGlzLnZvbHVtZSA9PSAwKSB7XHJcbiAgICAgICAgICB0aGlzLnZvbHVtZSA9IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucGxheWVyLnNldFZvbHVtZSh0aGlzLnZvbHVtZSk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICB9XHJcbiAgICB9LCB0aGlzLnRpbWVvdXQpXHJcbiAgfVxyXG5cclxuICBzZWVrVG8oc2Vjb25kcykge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuc2V0Q3VycmVudFRpbWUgPT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuc2V0Q3VycmVudFRpbWUoc2Vjb25kcyk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICB9XHJcbiAgICB9LCB0aGlzLnRpbWVvdXQpXHJcbiAgfVxyXG5cclxuICBzZXRWb2x1bWUobnVtYmVyKSB7XHJcbiAgICBsZXQgdGltZW91dCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgaWYgKHRoaXMucGxheWVyICYmIHR5cGVvZiB0aGlzLnBsYXllci5zZXRWb2x1bWUgPT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgY29uc3Qgdm9sdW1lID0gbnVtYmVyID4gMTAwID8gMSA6IG51bWJlciAvIDEwMDtcclxuXHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuc2V0Vm9sdW1lKHZvbHVtZSk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICB9XHJcbiAgICB9LCB0aGlzLnRpbWVvdXQpXHJcbiAgfVxyXG5cclxuICBzZXRMb29wKCBib29sICkge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuc2V0TG9vcCA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICB0aGlzLnBsYXllci5zZXRMb29wKCBib29sICk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICB9XHJcbiAgICB9LCB0aGlzLnRpbWVvdXQpXHJcbiAgfVxyXG5cclxuICBzZXRTaXplKHcsIGgpIHtcclxuICAgIGxldCB0aW1lb3V0ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5wbGF5ZXIgJiYgdHlwZW9mIHRoaXMucGxheWVyLmVsZW1lbnQgIT0gXCJ1bmRlZmluZWRcIikge1xyXG4gICAgICAgIHRoaXMucGxheWVyLmVsZW1lbnQuc2V0QXR0cmlidXRlKCd3aWR0aCcsIHcpO1xyXG4gICAgICAgIHRoaXMucGxheWVyLmVsZW1lbnQuc2V0QXR0cmlidXRlKCdoZWlnaHQnLCBoKTtcclxuICAgICAgICBjbGVhckludGVydmFsKHRpbWVvdXQpXHJcbiAgICAgIH1cclxuICAgIH0sIHRoaXMudGltZW91dClcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHZpbWVvIiwiaW1wb3J0IGJhc2UgZnJvbSAnLi9iYXNlJztcclxuXHJcbmNsYXNzIHlvdXR1YmUgZXh0ZW5kcyBiYXNlIHtcclxuICBpbml0KCkge1xyXG4gICAgdGhpcy5tYXRjaCA9IC8oeW91dHViZVxcLmNvbXx5b3V0dVxcLmJlfHlvdXR1YmVcXC1ub2Nvb2tpZVxcLmNvbSlcXC8od2F0Y2hcXD8oLiomKT92PXx2XFwvfHVcXC98ZW1iZWRcXC8/KT8odmlkZW9zZXJpZXNcXD9saXN0PSguKil8W1xcdy1dezExfXxcXD9saXN0VHlwZT0oLiopJmxpc3Q9KC4qKSkoLiopL2k7XHJcbiAgICB0aGlzLmlkTWF0Y2hJbmRleCA9IDQ7XHJcbiAgICB0aGlzLnVybCA9IFwiaHR0cHM6Ly93d3cueW91dHViZS1ub2Nvb2tpZS5jb20vZW1iZWQvJGlkP2VuYWJsZWpzYXBpPTFcIjtcclxuICAgIHRoaXMudHBsID0gXCI8aWZyYW1lICBzcmM9XFxcIiR1cmxcXFwiIGZyYW1lYm9yZGVyPVxcXCIwXFxcIiBhbGxvdz1cXFwiYWNjZWxlcm9tZXRlcjsgYXV0b3BsYXk7IGVuY3J5cHRlZC1tZWRpYTsgZ3lyb3Njb3BlOyBwaWN0dXJlLWluLXBpY3R1cmVcXFwiIHdlYmtpdGFsbG93ZnVsbHNjcmVlbiBtb3phbGxvd2Z1bGxzY3JlZW4gYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPlwiO1xyXG4gICAgdGhpcy5hcGkgPSBcImh0dHBzOi8vd3d3LnlvdXR1YmUuY29tL2lmcmFtZV9hcGlcIjtcclxuICAgIHRoaXMuaWQgPSBudWxsO1xyXG4gICAgdGhpcy5jb250YWluZXIgPSBudWxsO1xyXG4gICAgdGhpcy5wbGF5ZXIgPSBudWxsO1xyXG4gICAgdGhpcy50aW1lb3V0ID0gMTAwO1xyXG4gICAgdGhpcy5sb29wID0gMDtcclxuICAgIHRoaXMuc3RhdHVzID0ge1xyXG4gICAgICBsb2FkZWQ6IGZhbHNlXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBsb2FkKHBsdWdpbikge1xyXG4gICAgbGV0IGVsZW1lbnQ7XHJcbiAgICBsZXQgc3JjO1xyXG4gICAgbGV0IG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCBwbHVnaW4ub3B0aW9ucyk7XHJcbiAgICBpZiAocGx1Z2luLnZpZGVvIGluc3RhbmNlb2YgRWxlbWVudCB8fCBwbHVnaW4udmlkZW8gaW5zdGFuY2VvZiBIVE1MRG9jdW1lbnQpIHtcclxuICAgICAgZWxlbWVudCA9IHBsdWdpbi52aWRlbztcclxuICAgIH0gZWxzZSBpZiAodHlwZW9mIHBsdWdpbi52aWRlbyA9PSBcInN0cmluZ1wiKSB7XHJcbiAgICAgIGVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHBsdWdpbi52aWRlbyk7XHJcbiAgICB9XHJcbiAgICBpZiAoZWxlbWVudCkge1xyXG4gICAgICBpZiAoZWxlbWVudC50YWdOYW1lID09ICdJRlJBTUUnKSB7XHJcbiAgICAgICAgc3JjID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3NyYycpO1xyXG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiBvcHRpb25zLnVybCA9PSBcInN0cmluZ1wiKSB7XHJcbiAgICAgICAgc3JjID0gb3B0aW9ucy51cmw7XHJcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIG9wdGlvbnMudmlkZW9JZCA9PSBcInN0cmluZ1wiKSB7XHJcbiAgICAgICAgdGhpcy5pZCA9IG9wdGlvbnMudmlkZW9JZDtcclxuICAgICAgfVxyXG4gICAgICBpZiAoc3JjKSB7XHJcbiAgICAgICAgY29uc3QgciA9IHNyYy5tYXRjaCh0aGlzLm1hdGNoKVxyXG4gICAgICAgIGlmIChyKSB7XHJcbiAgICAgICAgICB0aGlzLmlkID0gclt0aGlzLmlkTWF0Y2hJbmRleF07XHJcbiAgICAgICAgICBvcHRpb25zLnZpZGVvSWQgPSByW3RoaXMuaWRNYXRjaEluZGV4XTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5pZCkge1xyXG4gICAgICBpZiAoZWxlbWVudCAmJiBlbGVtZW50LnRhZ05hbWUgPT0gJ0lGUkFNRScpIHtcclxuICAgICAgICB0aGlzLnZpZGVvID0gZWxlbWVudDtcclxuICAgICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnc3JjJywgdGhpcy51cmwucmVwbGFjZSgnJGlkJywgdGhpcy5pZCkpO1xyXG4gICAgICB9IGVsc2UgaWYgKGVsZW1lbnQpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IGVsZW1lbnQ7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHR5cGVvZiBZVCA9PSBcInVuZGVmaW5lZFwiIHx8ICFZVCkge1xyXG4gICAgICAgIGxldCBzY3JpcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic2NyaXB0XCIpO1xyXG4gICAgICAgIHNjcmlwdC50eXBlID0gXCJ0ZXh0L2phdmFzY3JpcHRcIjtcclxuICAgICAgICBzY3JpcHQuc3JjID0gdGhpcy5hcGk7XHJcbiAgICAgICAgd2luZG93Lm9uWW91VHViZUlmcmFtZUFQSVJlYWR5ID0gKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5zdGF0dXMubG9hZGVkID0gdHJ1ZTtcclxuICAgICAgICAgIGRlbGV0ZSBvcHRpb25zLnVybDtcclxuICAgICAgICAgIHRoaXMuZ2V0UGxheWVyKG9wdGlvbnMpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdClcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnN0YXR1cy5sb2FkZWQgPSB0cnVlO1xyXG4gICAgICAgIGRlbGV0ZSBvcHRpb25zLnVybDtcclxuICAgICAgICB0aGlzLmdldFBsYXllcihvcHRpb25zKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxuXHJcbiAgcGxheSgpIHtcclxuICAgIGxldCB0aW1lb3V0ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5wbGF5ZXIgJiYgdHlwZW9mIHRoaXMucGxheWVyLnBsYXlWaWRlbyA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICB0aGlzLnBsYXllci5wbGF5VmlkZW8oKTtcclxuICAgICAgICBjbGVhckludGVydmFsKHRpbWVvdXQpXHJcbiAgICAgIH1cclxuICAgIH0sIHRoaXMudGltZW91dClcclxuICB9XHJcblxyXG4gIHBhdXNlKCkge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIucGF1c2VWaWRlbyA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICB0aGlzLnBsYXllci5wYXVzZVZpZGVvKCk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICB9XHJcbiAgICB9LCB0aGlzLnRpbWVvdXQpXHJcbiAgfVxyXG5cclxuICBzdG9wKCkge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuc3RvcFZpZGVvID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMucGxheWVyLnN0b3BWaWRlbygpO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgZGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuZGVzdHJveSA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgdGhpcy5wbGF5ZXIuZGVzdHJveSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbXV0ZSgpIHtcclxuICAgIGxldCB0aW1lb3V0ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5wbGF5ZXIgJiYgdHlwZW9mIHRoaXMucGxheWVyLm11dGUgPT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIubXV0ZSgpO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgdW5NdXRlKCkge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIudW5NdXRlID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMucGxheWVyLnVuTXV0ZSgpO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgc2Vla1RvKHNlY29uZHMpIHtcclxuICAgIGxldCB0aW1lb3V0ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5wbGF5ZXIgJiYgdHlwZW9mIHRoaXMucGxheWVyLnNlZWtUbyA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICB0aGlzLnBsYXllci5zZWVrVG8oc2Vjb25kcyk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lb3V0KVxyXG4gICAgICB9XHJcbiAgICB9LCB0aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgc2V0Vm9sdW1lKG51bWJlcikge1xyXG4gICAgbGV0IHRpbWVvdXQgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnBsYXllciAmJiB0eXBlb2YgdGhpcy5wbGF5ZXIuc2V0Vm9sdW1lID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMucGxheWVyLnNldFZvbHVtZShudW1iZXIpO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgfVxyXG4gICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gIH1cclxuXHJcbiAgc2V0U2l6ZSh3LCBoKSB7XHJcbiAgICBsZXQgdGltZW91dCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgaWYgKHRoaXMucGxheWVyICYmIHR5cGVvZiB0aGlzLnBsYXllci5zZXRTaXplID09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMucGxheWVyLmEuc2V0QXR0cmlidXRlKCd3aWR0aCcsIHcpO1xyXG4gICAgICAgIHRoaXMucGxheWVyLmEuc2V0QXR0cmlidXRlKCdoZWlnaHQnLCBoKTtcclxuICAgICAgICBjbGVhckludGVydmFsKHRpbWVvdXQpXHJcbiAgICAgIH1cclxuICAgIH0sIHRoaXMudGltZW91dClcclxuICB9XHJcblxyXG4gIGdldFBsYXllcihvcHRpb25zKSB7XHJcbiAgICBpZiAodGhpcy5pZCkge1xyXG4gICAgICBvcHRpb25zLmV2ZW50cyA9IHtcclxuICAgICAgICAnb25TdGF0ZUNoYW5nZSc6IChlKSA9PiB7XHJcbiAgICAgICAgICBpZiAoWVQuUGxheWVyU3RhdGUuRU5ERUQgPT0gZS5kYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZG9Mb29wKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGxldCB0aW1lb3V0ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnN0YXR1cy5sb2FkZWQpIHtcclxuICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGltZW91dClcclxuICAgICAgICAgIGlmICh0aGlzLnZpZGVvKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGxheWVyID0gbmV3IFlULlBsYXllcih0aGlzLnZpZGVvLCBvcHRpb25zKTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5jb250YWluZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5wbGF5ZXIgPSBuZXcgWVQuUGxheWVyKHRoaXMuY29udGFpbmVyLCBvcHRpb25zKTtcclxuICAgICAgICAgICAgdGhpcy52aWRlbyA9IHRoaXMucGxheWVyLmE7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfSwgdGhpcy50aW1lb3V0KVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2V0TG9vcChib29sKSB7XHJcbiAgICBsZXQgdGltZW91dCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgaWYgKHRoaXMucGxheWVyKSB7XHJcbiAgICAgICAgdGhpcy5sb29wID0gYm9vbDtcclxuICAgICAgICBjbGVhckludGVydmFsKHRpbWVvdXQpXHJcbiAgICAgIH1cclxuICAgIH0sIHRoaXMudGltZW91dClcclxuICB9XHJcblxyXG4gIGRvTG9vcCgpIHtcclxuICAgIGlmICh0aGlzLnBsYXllciAmJiB0aGlzLmxvb3ApIHtcclxuICAgICAgdGhpcy5wbGF5ZXIuc2Vla1RvKDApO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgeW91dHViZSJdLCJzb3VyY2VSb290IjoiIn0=