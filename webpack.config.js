const debug = process.env.NODE_ENV !== 'production';
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');

module.exports = {
  devtool: debug ? 'inline-source-map' : 'hidden-source-map',
  mode: debug ? 'development' : 'production',
  entry: {
    main: "./src/index.js",
    navideoapi: "./src/dev/navideoapi.js",
  },
  output: {
    path: path.resolve( path.resolve(__dirname),),
    filename: (chunkData) => {
      let prefix = debug ? '' : '.min';
      return 'js/[name]' + prefix + '.js'
    }
  },
  optimization: {
    providedExports: false,
    minimizer: [
      new UglifyJsPlugin({
      extractComments: true,
    })
    ],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(scss|sass)$/,
        include: [
          path.resolve(__dirname, '/'),
     //     path.resolve(__dirname, 'layouts/')
        ],
        exclude: /node_modules/,
        use: [
          {
            // Adds CSS to the DOM by injecting a `<style>` tag
            loader: 'style-loader'
          },
          {
            // Interprets `@import` and `url()` like `import/require()` and will resolve them
            loader: 'css-loader'
          },
          {
            // Loader for webpack to process CSS with PostCSS
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [
                  require('autoprefixer')
                ];
              }
            }
          },
          {
            // Loads a SASS/SCSS file and compiles it to CSS
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|php)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: "[path][name].[ext]",
            },
          }
        ]
      }
    ]
  },
  externals: {
    'react': 'React',
    'react-dom': 'ReactDOM',
  },
  plugins: [
    new BrowserSyncPlugin({
      proxy: "http://naVideoApi.test/src",
      host: 'naVideoApi.test',
      open: 'external',
    }),
  ],
};