import React from 'react'
import ReactDOM from 'react-dom'

jQuery(() => {
  const video = new naVideoApi( '#vimeo', {
    playerVars: {
      controls: 0,
      disablekb: 1,
      enablejsapi: 1,
      modestbranding: 1,
      showinfo: 0,
    }
  });
  video.setLoop();
  const videoY = new naVideoApi( '#youtube', {
    playerVars: {
      controls: 0,
      disablekb: 1,
      enablejsapi: 1,
      modestbranding: 1,
      showinfo: 0,
    }
  });
  videoY.setLoop();
  const actions = [
    'setSize',
    'play',
    'pause',
    'play',
    'stop',
    'play',
    'mute',
    'unMute',
    'seekTo',
    'setVolume',
  ]
  let i = 0;
  const timer = setInterval(() => {
    if (i < actions.length) {
      const action = actions[i++];
      ReactDOM.render( action, document.getElementsByClassName( 'action-note' )[0])
      if ( action == 'seekTo' ) {
        video[action]( 90 );
        videoY[action]( 90 );
      } else if ( action == 'setVolume' ) {
        video[action]( 40 );
        videoY[action]( 40 );
      } else if ( action == 'setSize' ) {
        video[action]( 300, 300 );
        videoY[action]( 300, 300 );
      } else {
        video[action]();
        videoY[action]();
      }
    } else {
     clearInterval( timer );
    }
  }, 5000)
})