import base from './base';

class youtube extends base {
  init() {
    this.match = /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i;
    this.idMatchIndex = 4;
    this.url = "https://www.youtube-nocookie.com/embed/$id?enablejsapi=1";
    this.tpl = "<iframe  src=\"$url\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
    this.api = "https://www.youtube.com/iframe_api";
    this.id = null;
    this.container = null;
    this.player = null;
    this.timeout = 100;
    this.loop = 0;
    this.status = {
      loaded: false
    }
  }

  load(plugin) {
    let element;
    let src;
    let options = Object.assign({}, plugin.options);
    if (plugin.video instanceof Element || plugin.video instanceof HTMLDocument) {
      element = plugin.video;
    } else if (typeof plugin.video == "string") {
      element = document.querySelector(plugin.video);
    }
    if (element) {
      if (element.tagName == 'IFRAME') {
        src = element.getAttribute('src');
      } else if (typeof options.url == "string") {
        src = options.url;
      } else if (typeof options.videoId == "string") {
        this.id = options.videoId;
      }
      if (src) {
        const r = src.match(this.match)
        if (r) {
          this.id = r[this.idMatchIndex];
          options.videoId = r[this.idMatchIndex];
        }
      }
    }

    if (this.id) {
      if (element && element.tagName == 'IFRAME') {
        this.video = element;
        element.setAttribute('src', this.url.replace('$id', this.id));
      } else if (element) {
        this.container = element;
      }
      if (typeof YT == "undefined" || !YT) {
        let script = document.createElement("script");
        script.type = "text/javascript";
        script.src = this.api;
        window.onYouTubeIframeAPIReady = () => {
          this.status.loaded = true;
          delete options.url;
          this.getPlayer(options);
        }
        document.body.appendChild(script)
      } else {
        this.status.loaded = true;
        delete options.url;
        this.getPlayer(options);
      }
      return this;
    }
    return null;
  }

  play() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.playVideo == "function") {
        this.player.playVideo();
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  pause() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.pauseVideo == "function") {
        this.player.pauseVideo();
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  stop() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.stopVideo == "function") {
        this.player.stopVideo();
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  destroy() {
    if (this.player && typeof this.player.destroy == "function") {
      this.player.destroy();
    }
  }

  mute() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.mute == "function") {
        this.player.mute();
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  unMute() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.unMute == "function") {
        this.player.unMute();
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  seekTo(seconds) {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.seekTo == "function") {
        this.player.seekTo(seconds);
        clearInterval(timeout)
      }
    }, timeout)
  }

  setVolume(number) {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.setVolume == "function") {
        this.player.setVolume(number);
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  setSize(w, h) {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.setSize == "function") {
        this.player.a.setAttribute('width', w);
        this.player.a.setAttribute('height', h);
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  getPlayer(options) {
    if (this.id) {
      options.events = {
        'onStateChange': (e) => {
          if (YT.PlayerState.ENDED == e.data) {
            this.doLoop();
          }
        }
      }
      let timeout = setInterval(() => {
        if (this.status.loaded) {
          clearInterval(timeout)
          if (this.video) {
            this.player = new YT.Player(this.video, options);
          } else if (this.container) {
            this.player = new YT.Player(this.container, options);
            this.video = this.player.a;
          }

        }
      }, this.timeout)
    }
  }

  setLoop(bool) {
    let timeout = setInterval(() => {
      if (this.player) {
        this.loop = bool;
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  doLoop() {
    if (this.player && this.loop) {
      this.player.seekTo(0);
    }
  }
}

export default youtube