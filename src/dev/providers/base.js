class base {
  init() {
    this.match = /^([\d]+)?/;
    this.idMatchIndex = 1;
    this.url = "https://www.example.com/$id?api=1";
    this.tpl = "<iframe  src=\"$url\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
    this.api = "https://www.example.com/api";
    this.id = null;
    this.container = null;
    this.player = null;
    this.status = {
      loaded: false,
    };
  }

  constructor() {
    this.init();
  }

  load(plugin) {
    let element;
    let src;
    let options =  Object.assign({}, plugin.options);
    if (plugin.video instanceof Element || plugin.video instanceof HTMLDocument) {
      element = plugin.video;
    } else if (typeof plugin.video == "string") {
      element = document.querySelector(plugin.video);
    }
    if (element) {
      if (element.tagName == 'IFRAME') {
        src = element.getAttribute('src');
      } else if (typeof options.url == "string") {
        src = options.url;
      } else if (typeof options.videoId == "string") {
        this.id = options.videoId;
      }
      if (src) {
        const r = src.match(this.match)
        if (r) {
          this.id = r[this.idMatchIndex];
          options.videoId = r[this.idMatchIndex];
        }
      }
    }

    if (this.id) {
      if (element && element.tagName == 'IFRAME') {
        this.video = element;
        element.setAttribute('src', this.url.replace('$id', this.id));
      } else if (element) {
        this.container = element;
      }
      if (!this.class || typeof window[this.class] == "undefined" || !window[this.class]) {
        let script = document.createElement("script");
        script.type = "text/javascript";
        script.src = this.api;
        script.onload = () => {
          this.status.loaded = true;
          delete options.url;
          this.getPlayer(options);
        }
        document.body.appendChild(script)
      } else {
        this.status.loaded = true;
        delete options.url;
        this.getPlayer(options);
      }
      return this;
    }
    return null;
  }


  play() {
  }


  pause() {
  }


  stop() {
  }


  mute() {
  }

  destroy() {
  }


  unMute() {
  }

  seekTo(seconds) {
  }


  setVolume(number) {
  }

  setSize(w, h) {
  }

  setLoop(bool) {
  }


  getPlayer(options) {
  }
}

export default base