import base from './base';

class vimeo extends base {

  init() {
    this.match = /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/;
    this.idMatchIndex = 2;
    this.url = "https://player.vimeo.com/video/$id?api=1";
    this.tpl = "<iframe  src=\"$url\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
    this.api = "https://player.vimeo.com/api/player.js";
    this.class = "https://player.vimeo.com/api/player.js";
    this.id = null;
    this.container = null;
    this.player = null;
    this.timeout = 100;
    this.volume = 0;
    this.status = {
      loaded: false
    };
  }

  getPlayer(options) {
    if (this.id) {
      let timeout = setInterval(() => {

        if (this.status.loaded) {
          clearInterval(timeout)
          if (this.video) {
            this.player = new Vimeo.Player(this.video, options);
          } else if (this.container) {
            options.id = this.id;
            this.player = new Vimeo.Player(this.container, options);
            this.video = this.player.element;
          }

        }
      }, this.timeout)
    }
  }

  play() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.play == "function") {
        this.player.play();
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  destroy() {
    if (this.player && typeof this.player.destroy == "function") {
      this.player.destroy();
    }
  }

  pause() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.pause == "function") {
        this.player.pause();
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  stop() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.pause == "function") {
        this.player.pause();
        this.player.setCurrentTime(0);
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  mute() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.setVolume == "function") {
        this.player.getVolume().then((volume) => {
          this.volume = volume;
        });
        this.player.setVolume(0);
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  unMute() {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.setVolume == "function") {
        if (this.volume == 0) {
          this.volume = 1;
        }
        this.player.setVolume(this.volume);
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  seekTo(seconds) {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.setCurrentTime == "function") {
        this.player.setCurrentTime(seconds);
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  setVolume(number) {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.setVolume == "function") {
        const volume = number > 100 ? 1 : number / 100;

        this.player.setVolume(volume);
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  setLoop( bool ) {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.setLoop == "function") {
        this.player.setLoop( bool );
        clearInterval(timeout)
      }
    }, this.timeout)
  }

  setSize(w, h) {
    let timeout = setInterval(() => {
      if (this.player && typeof this.player.element != "undefined") {
        this.player.element.setAttribute('width', w);
        this.player.element.setAttribute('height', h);
        clearInterval(timeout)
      }
    }, this.timeout)
  }
}

export default vimeo