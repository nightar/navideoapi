import youtube from "./providers/youtube";
import vimeo from "./providers/vimeo";

((w) => {
  const naVideoApiData = {
    youtube,
    vimeo
  }

  w.naVideoApi = function (video, options) {
    if (video) {
      this.video = video
      this.options = options;
      this.init();
      return this;
    } else {
      return null;
    }
  }
  naVideoApi.prototype = {

    provider: null,
    /**
     * Init provider
     */
    init: function () {
      for (var providerName in naVideoApiData) {
        const provider = new naVideoApiData[providerName]();
        if (provider.load( this )) {
          this.provider = provider;
        }

      }
    },
    /**
     * Get html video
     */
    getTpl: function () {
      if (this.provider && this.provider.tpl && this.embededUrl) {
        return this.provider.tpl.replace('$url', this.embededUrl);
      }
    },
    /**
     * Video controls
     */
    play: function () {
      if (this.provider) {

        this.provider.play();
      }
    },
    pause: function () {
      if (this.provider) {
        this.provider.pause();
      }
    },
    stop: function () {
      if (this.provider) {
        this.provider.stop();
      }
    },
    mute: function () {
      if (this.provider) {
        this.provider.mute();
      }
    },
    unMute: function () {
      if (this.provider) {
        this.provider.unMute();
      }
    },
    destroy: function () {
      if (this.provider) {
        this.provider.destroy();
      }
      this.options = null;
    },
    seekTo: function (seconds) {
      if (!seconds) {
        seconds = 0;
      }
      if (this.provider) {
        this.provider.seekTo(seconds);
      }
    },
    setVolume: function (level) {
      if (!level) {
        level = 1;
      }
      if (this.provider) {
        this.provider.setVolume(level);
      }
    },
    setSize: function (w, h) {
      if (!w) {
        w = 340;
      }
      if (!h) {
        h = 480;
      }
      if (this.provider) {
        this.provider.setSize(w, h);
      }
    },
    setLoop: function ( bool = true ) {
      if (this.provider) {
        this.provider.setLoop( bool );
      }
    }

  }
})(window)

export default naVideoApi